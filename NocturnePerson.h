#ifdef __MACOSX_CORE__
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
  #include <GL/glut.h>
#endif

#include "NocturneObject.h"
#include "NocturneConstants.h"
#include "Timing.h"
#include "Room.h"
#include <math.h>

#ifndef NOCTURNE_PERSON_H
#define NOCTURNE_PERSON_H

#define NUM_PERSON_POINTS 32
#define PERSON_RADIUS 0.1

#define COLOR_FADE_SPEED 1.0

#define MAX_VELOCITY (15.0 * PERSON_RADIUS)
#define DEPRESSED_VELOCITY (5.0 * PERSON_RADIUS)
#define ACCEL_PER_SECOND (30.0 * PERSON_RADIUS)
#define WALL_ELASTICITY 0.8

#define COLLISION_FP_FIX (0.001 * PERSON_RADIUS)

#define MOVE_COMMAND_MARGIN_OF_ERROR (0.5 * PERSON_RADIUS)

#define ROOM_OFFSET (ROOM_SIZE / 2.0)

class NocturnePerson: public NocturneObject {
public:
	NocturnePerson();
	virtual void draw();
	double getRadius();
	double getSanity();
	virtual NocturneAilment ailment();
	void setLocation(GLfloat absolute_x, GLfloat absolute_y);
	void setVel(double x_vel, double y_vel);

	void updateCurrentRoom();

	friend struct NocturneObjectMoveCommand;

protected:
	double x_vel, y_vel;
	/*double accel_strength;
	double accel_dir[2];*/
	double x_accel, y_accel;
	double radius;

	double color[3];
	double restingColor[3];
	double colorFadeSpeed;

	double sanity;

	double lastTime;

	bool collisionsEnabled;
	bool canChangeRooms;
	bool canGenerateArea;

	NocturneAilment myAilment;

	Room *oldRoom;

	virtual void computeBehavior(double timeLapsed);
	virtual void collisionBehavior(NocturnePerson* otherPerson, double collisionStrength);
	virtual void onRoomChange(Room* newRoom);

	void checkAndHandleCollision(NocturnePerson* otherPerson);
	void checkAndHandleWallCollision();
	bool resolveCollision(NocturnePerson *otherPerson);
	double* breakVelTowardsPoint(double x, double y);
	void assembleVelTowardsPoint(double x, double y, double* brokenVel);

	double stopX();
	double stopY();
};

#endif