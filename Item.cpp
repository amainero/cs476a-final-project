#include "Item.h"

#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

enum ItemReadingMode {
	ITEM_ID,
	ITEM_NAME,
	ITEM_TYPE,
	ITEM_DESCRIPTION
};


Item::Item(string filename) {
	readFile(filename);
}


void Item::readFile(string filename) {
	ItemReadingMode reading_mode = ITEM_ID;
	ifstream input_stream(filename);
	string line;

	while (getline(input_stream, line)) {
		if (line.length() == 0) {
			continue;
		}

		if (line[0] == '#') {
			continue;
		}

		if (line == "ID") {
			reading_mode = ITEM_ID;
			continue;
		} else if (line == "NAME") {
			reading_mode = ITEM_NAME;
			continue;
		} else if (line == "TYPE") {
			reading_mode = ITEM_TYPE;
			continue;
		} else if (line == "DESCRIPTION") {
			reading_mode = ITEM_DESCRIPTION;
			continue;
		}

		switch (reading_mode) {
			case ITEM_ID:
				readID(line);
				continue;
			case ITEM_NAME:
				readName(line);
				continue;
			case ITEM_TYPE:
				readType(line);
				continue;
			case ITEM_DESCRIPTION:
				readDescription(line);
				continue;
		}
	}
}


void Item::readID(string line) {
	this->id = line;
}


void Item::readName(string line) {
	this->name = line;
}


void Item::readType(string line) {
	if (line == "KEY") {
		item_type = KEY;
	} else if (line == "BOOK") {
		item_type = BOOK;
	} else if (line == "PICTURE") {
		item_type = PICTURE;
	} else if (line == "TRINKET") {
		item_type = TRINKET;
	} else if (line == "WEAPON") {
		item_type = WEAPON;
	} else {
		cout << "Error reading item type: " << line << endl;
	}
}


void Item::readDescription(string line) {
	this->description = line;
}