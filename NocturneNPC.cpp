#include "NocturneNPC.h"

map<string, NocturneNPC*> NocturneNPC::npcMap;

NocturneNPC::NocturneNPC(string id, NocturneAilment ailment, double color[3]) {
	this->id = id;
	NocturneNPC::npcMap[id] = this;
	myAilment = ailment;
	this->color[0] = color[0];
	this->color[1] = color[1];
	this->color[2] = color[2];
	this->restingColor[0] = color[0];
	this->restingColor[1] = color[1];
	this->restingColor[2] = color[2];
}

NocturneNPC::~NocturneNPC() {
	NocturneNPC::npcMap[id] = NULL;
}

void NocturneNPC::cure() {
	myAilment = NO_AILMENT;
	restingColor[0] = 1.0;
	restingColor[1] = 1.0;
	restingColor[2] = 1.0;
}

string NocturneNPC::getID() { return id; }

NocturneNPC* NocturneNPC::getNPC(string id) {
	return NocturneNPC::npcMap[id];
}