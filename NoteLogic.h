#ifndef NOTE_LOGIC_H
#define NOTE_LOGIC_H

#include <string>
#include <vector>
#include "RandomGenerator.h"

#define NUM_SEMITONES_IN_OCTAVE 12
#define MIDI_ROOT_KEY 57 //9 // Change back when octave generation is fixed.

struct Note {
	// absolute semitones start at A3 = 0
	unsigned int semitone;
};


struct Chord {
	std::vector<struct Note> notes;
};


struct ChordProgression {
	std::vector<struct Chord> chords;
};


class NoteLogic {
public:
	static double getFrequencyForNote(Note *note);
	// static std::vector<Note *> generateNotesForNewRoom();
	// static std::string getChordNameForNotes(std::vector<Note *> notes);
	static int getMIDIKeyForNote(Note note);

private:
	// static int generateRandomChordType();
	// static int generateRandomModeType();
	// static int generateRandomOctave();
	// static int getModeStep(int mode_type);
};

#endif