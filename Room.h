#ifndef ROOM_H
#define ROOM_H

#include <vector>
#include "Resident.h"
#include "Item.h"
#include "NocturneNPC.h"

#define ROOM_SIZE 1.7
#define DOOR_SIZE (ROOM_SIZE / 3)
#define DOOR_START_DIST ((ROOM_SIZE / 2) - (DOOR_SIZE / 2))
#define DOOR_END_DIST (DOOR_START_DIST + DOOR_SIZE)

#define RUG_START_DIST (DOOR_START_DIST / 2)
#define RUG_END_DIST (ROOM_SIZE - RUG_START_DIST)

#define ROOM_DRAW_OFFSET_X (1.5 * ROOM_SIZE)
#define ROOM_DRAW_OFFSET_Y (0.5 * ROOM_SIZE)

enum Direction { NORTH, EAST, WEST, SOUTH };

class Room {
public:

	Room(
		int area_absolute_x,
		int area_absolute_y,
		int area_relative_x, 
		int area_relative_y
	);
	void setWall(Direction dir, bool is_open);

	void createInitialResidentsFromNotes(std::vector<Note> notes);

	std::vector<Resident *> getResidents();
	std::vector<Resident *> getTargetResidents();

	bool containsTargetResidents();

	void addResident(Resident *resident);
	void removeResident(Resident *resident);
	void setResidentRandomLocation(Resident * resident);

	int getAbsoluteX();
	int getAbsoluteY();

	int getRelativeX();
	int getRelativeY();

	bool wallIsOpen(Direction dir);

	// quest methods
	void setLocked(bool is_locked);
	bool isLocked();
	
	bool canBeUnlocked(NocturneNPC *npc, Item *item);
	void setUnlockConditions(NocturneNPC *npc, Item *item);

	void draw(float color[3]);
	void drawLockIndicators();

private:

	int area_absolute_x, area_absolute_y;
	int area_relative_x, area_relative_y;
	std::vector<Resident *> current_residents;
	std::vector<Resident *> target_residents;
	bool north_wall_open, east_wall_open;
	bool south_wall_open, west_wall_open;

	NocturneNPC *unlock_condition_npc;
	Item *unlock_condition_item;

	bool is_locked;
};

#endif