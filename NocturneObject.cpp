#include "NocturnePerson.h"
#include "Board.h"
#include "Room.h"
#include "Area.h"
#include <iostream>

using namespace std;

Board* NocturneObject::theBoard = NULL;
AssetManager* NocturneObject::assetManager = NULL;

NocturneObjectMoveCommand::NocturneObjectMoveCommand(NocturneObject* p, double x, double y, double commandTime, NocturneObjectMoveMethod method) {
	goal_x = x;
	goal_y = y;
	this->start_x = p->x_loc;
	this->start_y = p->y_loc;
	timeLeft = commandTime;
	timeForCommand = commandTime;
	this->method = method;
	this->object = p;

	// CORRECT FOR PHYSICS
	if (method == PHYSICS) {
		if (start_x < goal_x) goal_x += MOVE_COMMAND_MARGIN_OF_ERROR;
		else if (start_x > goal_x) goal_x -= MOVE_COMMAND_MARGIN_OF_ERROR;
		if (start_y < goal_y) goal_y += MOVE_COMMAND_MARGIN_OF_ERROR;
		else if (start_y > goal_y) goal_y -= MOVE_COMMAND_MARGIN_OF_ERROR;
	}
}

bool NocturneObjectMoveCommand::updateAndCheckFinish(double timeLapsed) {
	if (method == LINEAR) {
		double xDist = goal_x - object->x_loc;
		double yDist = goal_y - object->y_loc;

		xDist *= timeLapsed / timeLeft;
		yDist *= timeLapsed / timeLeft;

		if (timeLapsed >= timeLeft) {
			object->x_loc = goal_x;
			object->y_loc = goal_y;
			return true;
		}
		else {
			object->x_loc += xDist;
			object->y_loc += yDist;
			timeLeft -= timeLapsed;
			return false;
		}
	}

	if (method == SINE) {
		timeLeft = max(0.0, timeLeft - timeLapsed);
		double portionDone = (1.0 - cos((timeForCommand - timeLeft) / (timeForCommand) * PI)) / 2.0;
		object->x_loc = start_x + (goal_x - start_x) * portionDone;
		object->y_loc = start_y + (goal_y - start_y) * portionDone;
		if (timeLeft == 0.0) return true;
		else return false;
	}

	if (method == PHYSICS) {
		NocturnePerson* person = (NocturnePerson*) object;
		if (person->stopX() < goal_x - MOVE_COMMAND_MARGIN_OF_ERROR) person->x_accel = ACCEL_PER_SECOND;
		else if (person->stopX() > goal_x + MOVE_COMMAND_MARGIN_OF_ERROR) person->x_accel = -ACCEL_PER_SECOND;
		else person->x_accel = 0.0;
		if (person->stopY() < goal_y - MOVE_COMMAND_MARGIN_OF_ERROR) person->y_accel = ACCEL_PER_SECOND;
		else if (person->stopY() > goal_y + MOVE_COMMAND_MARGIN_OF_ERROR) person->y_accel = -ACCEL_PER_SECOND;
		else person->y_accel = 0.0;

		if (person->x_accel == 0.0 && person->y_accel == 0.0) return true;
		else return false;
	}

	return false;
}

NocturneObject::NocturneObject() {
	x_loc = 0.0;
	y_loc = 0.0;
	lastTime = currentTime();
	currentMoveCommand = NULL;
}

void NocturneObject::commandToRelativeLoc(double x, double y, double commandTime, NocturneObjectMoveMethod method) {
	commandToLoc(x + x_loc, y + y_loc, commandTime, method);
}

void NocturneObject::commandToLoc(double x, double y, double commandTime, NocturneObjectMoveMethod method) {
	if (currentMoveCommand) delete currentMoveCommand;
	currentMoveCommand = new NocturneObjectMoveCommand(this, x, y, commandTime, method);
}

void NocturneObject::computeFrame() {
	double thisTime = currentTime();
	double timeLapsed = thisTime - lastTime;
	computeBehavior(timeLapsed);
	lastTime = thisTime;
}

void NocturneObject::computeBehavior(double timeLapsed) {
	// Compute move command
	if (currentMoveCommand) {
		if (currentMoveCommand->updateAndCheckFinish(timeLapsed)) {
			delete currentMoveCommand;
			currentMoveCommand = NULL;
		}
	}
}

void NocturneObject::getScreenCoords(int* data) {

	int viewport[4];
	double mvmatrix[16],projmatrix[16];
	GLdouble window[3];

	glGetIntegerv(GL_VIEWPORT,viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, mvmatrix);
	glGetDoublev(GL_PROJECTION_MATRIX,projmatrix);

	gluProject((GLdouble)x_loc,(GLdouble)y_loc,(GLdouble)0.0,mvmatrix,projmatrix,viewport,window,window+1,window+2);

	data[0] = round(window[0]);
	data[1] = round(window[1]);
}

void NocturneObject::getScreenCoordsForPoint(double x, double y, int* data) {

	int viewport[4];
	double mvmatrix[16],projmatrix[16];
	GLdouble window[3];

	glGetIntegerv(GL_VIEWPORT,viewport);
	glGetDoublev(GL_MODELVIEW_MATRIX, mvmatrix);
	glGetDoublev(GL_PROJECTION_MATRIX,projmatrix);

	gluProject((GLdouble)x,(GLdouble)y,(GLdouble)0.0,mvmatrix,projmatrix,viewport,window,window+1,window+2);

	data[0] = round(window[0]);
	data[1] = round(window[1]);
}

Room* NocturneObject::getRoom() {
	if (NocturneObject::theBoard == NULL) return NULL;
	return NocturneObject::theBoard->getRoomFromRawCoordinates(x_loc, y_loc);
}
Area* NocturneObject::getArea() {
	if (NocturneObject::theBoard == NULL) return NULL;
	return NocturneObject::theBoard->getAreaFromRawCoordinates(x_loc, y_loc);
}

double NocturneObject::getX() { return x_loc; }
double NocturneObject::getY() { return y_loc; }