#ifndef AREA_SPECIFICATION_H
#define AREA_SPECIFICATION_H

#include "RandomGenerator.h"
#include "Room.h"
#include "NoteLogic.h"
#include "NocturneConstants.h"

#include <string>

#define GRID_DIM_SIZE 3

class AreaSpecification {
public:

	AreaSpecification(std::string filename);
	AreaSpecification(AreaSpecification *unrotated_specification);

	std::string getID();
	bool roomExists(int relative_x, int relative_y);
	bool roomIsLocked(int relative_x, int relative_y);

	// includes NPC notes
	Chord getInitialRoomChordDefinition(int relative_x, int relative_y);

	// instance variables
	std::string id;
	int locked_room_x;
	int locked_room_y;
	int num_rooms;

	bool room_exists[GRID_DIM_SIZE][GRID_DIM_SIZE];
	Chord initial_room_chord_definitions[GRID_DIM_SIZE][GRID_DIM_SIZE];

private:

	// instance methods
	void readFile(std::string filename);
	void readID(std::string line);
	void readRoomRow(std::string line, int current_room_row);
	void readLockedRoom(std::string line);
};

#endif