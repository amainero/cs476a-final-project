#ifndef NOCTURNE_CONSTANTS_H
#define NOCTURNE_CONSTANTS_H

/*
* AUDIO CONSTANTS
*/
// buffer information
#define SAMPLE float
#define NUM_BUFFER_FRAMES 1024
#define AUDIO_FORMAT RTAUDIO_FLOAT32
#define AUDIO_SAMPLE_RATE 44100
#define NUM_AUDIO_CHANNELS 2
#define SECONDS_IN_MINUTE 60

// pi
#define PI 3.14159265358979
#define TWOPI (2 * PI)

struct BufferWrapper {
	SAMPLE buffer[NUM_BUFFER_FRAMES];
};

/*
* GAME CONSTANTS
*/

enum NocturneQuestType { RELATIONSHIP, ITEM, NO_QUEST };
enum NocturneAilment { DEPRESSION, SCHIZOPHRENIA, AMNESIA, NO_AILMENT };

#endif
