//-----------------------------------------------------------------------------
// name: arcanum.cpp
//
// Stanford University | Music 256a: Music, Computing, and Design
//     http://ccrma.stanford.edu/courses/256a/
//-----------------------------------------------------------------------------
#include "NocturneConstants.h"
#include "RtAudio.h"
#include "chuck_fft.h"
#include "NoteLogic.h"
#include "Board.h"
#include "Resident.h"
#include "y-fluidsynth.h"
#include "Keeper.h"
#include "NocturneCamera.h"
#include "NocturneMessage.h"
#include "AssetManager.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#ifdef __MACOSX_CORE__
  // note: for mac only
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
  #include <GL/glut.h>
#endif

#include <iostream>
using namespace std;

//-----------------------------------------------------------------------------
// CONSTANTS
//-----------------------------------------------------------------------------

#define NOTE_DOT_RADIUS 0.04
#define NOTE_DOT_NUM_POINTS 32

#define DEFAULT_WIDTH 1600
#define DEFAULT_HEIGHT 1200

#define SHOW_INTRO true
#define FULL_SCREEN true


//-----------------------------------------------------------------------------
// GLOBAL GAME VARIABLES
//-----------------------------------------------------------------------------
Board *g_board;
NocturneCamera *g_camera;
AssetManager *g_asset_manager;

//-----------------------------------------------------------------------------
// GLOBAL AUDIO VARIABLES
//-----------------------------------------------------------------------------
unsigned int g_bufferFrames = 1024;
RtAudio audio;


//-----------------------------------------------------------------------------
// function prototypes
//-----------------------------------------------------------------------------
void initGraphics(int argc, char ** argv);
void idleFunc( );
void displayFunc( );
void reshapeFunc( GLsizei width, GLsizei height );
void keyboardFunc( unsigned char, int, int );
void mouseFunc( int button, int state, int x, int y );

void drawBoard();

void initAudio();

int callme( void * outputBuffer, void * inputBuffer, unsigned int numFrames,
            double streamTime, RtAudioStreamStatus status, void * data )
{
    Resident::processEvents();
    Resident::synthesize((SAMPLE*)outputBuffer, numFrames);
    return 0;
}


// entry point
int main( int argc, char ** argv )
{
    initAudio();

    g_asset_manager = new AssetManager();
    
    new Keeper();
    g_board = new Board();
    
    NocturneObject::theBoard = g_board;

    g_camera = new NocturneCamera();
    NocturneMessage::theCamera = g_camera;
    initGraphics(argc, argv);

    if (SHOW_INTRO) g_camera->fadeUpAfterDelay(NocturneMessage::intro() - 6.0);
    else g_camera->fadeUpAfterDelay(0.0);

    glutMainLoop();
    return 0;
}


void initGraphics(int argc, char ** argv) {
    // initialize GLUT
    glutInit( &argc, argv );
    // double buffer, use rgb color, enable depth buffer
    //glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB);
    // initialize the window size
    glutInitWindowSize( g_camera->windowWidth, g_camera->windowHeight );
    // set the window postion
    glutInitWindowPosition( 100, 100 );
    // create the window
    glutCreateWindow( "Keeper of The Nocturne" );
    
    // set the idle function - called when idle
    glutIdleFunc( idleFunc );
    // set the display function - called when redrawing
    glutDisplayFunc( displayFunc );
    // set the reshape function - called when client area changes
    glutReshapeFunc( reshapeFunc );
    // set the keyboard function - called on keyboard events
    //glutKeyboardFunc( keyboardFunc );
    glutKeyboardFunc(Keeper::keyboardCallback);
    glutKeyboardUpFunc(Keeper::keyboardUpCallback);
    // set the mouse function - called on mouse stuff
    glutMouseFunc( mouseFunc );

    if (FULL_SCREEN) {
        glutFullScreen();
    }

    // Enable alpha.
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void initAudio() {
    // variables
    unsigned int bufferBytes = 0;
    
    // check for audio devices
    if( audio.getDeviceCount() < 1 )
    {
        // nopes
        cout << "no audio devices found!" << endl;
        exit( 1 );
    }

    // let RtAudio print messages to stderr.
    audio.showWarnings( true );
    
    // set input and output parameters
    RtAudio::StreamParameters iParams, oParams;
    iParams.deviceId = audio.getDefaultInputDevice();
    iParams.nChannels = NUM_AUDIO_CHANNELS;
    iParams.firstChannel = 0;
    oParams.deviceId = audio.getDefaultOutputDevice();
    oParams.nChannels = NUM_AUDIO_CHANNELS;
    oParams.firstChannel = 0;
    
    // create stream options
    RtAudio::StreamOptions options;
    
    // go for it
    try {
        // open a stream
        audio.openStream( &oParams, &iParams, AUDIO_FORMAT, AUDIO_SAMPLE_RATE, &g_bufferFrames, &callme, (void *)&bufferBytes, &options );
    }
    catch( RtError& e )
    {
        // error!
        cout << e.getMessage() << endl;
        exit( 1 );
    }

    try {
        // start stream
        audio.startStream();
    }
    catch( RtError& e )
    {
        // print error message
        cout << e.getMessage() << endl;
    }

    // Init residents.
    Resident::loadResidentSynth();
}


//-----------------------------------------------------------------------------
// Name: reshapeFunc( )
// Desc: called when window size changes
//-----------------------------------------------------------------------------
void reshapeFunc( GLsizei w, GLsizei h )
{
    // save the new window size
    g_camera->windowWidth = w; g_camera->windowHeight = h;
    // map the view port to the client area
    glViewport( 0, 0, w, h );
    // set the matrix mode to project
    glMatrixMode( GL_PROJECTION );
    // load the identity matrix
    glLoadIdentity( );
    // create the viewing frustum
    gluPerspective( 45.0, (GLfloat) w / (GLfloat) h, 1.0, 300.0 );
    // set the matrix mode to modelview
    glMatrixMode( GL_MODELVIEW );
    // load the identity matrix
    glLoadIdentity( );
    // position the view point
    gluLookAt( 0.0f, 0.0f, 10.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f );
}



//-----------------------------------------------------------------------------
// Name: keyboardFunc( )
// Desc: key event
//-----------------------------------------------------------------------------
void keyboardFunc( unsigned char key, int x, int y )
{
    switch( key )
    {
        case 'Q':
        case 'q':
            exit(1);
            break;
    }
    
    glutPostRedisplay( );
}




//-----------------------------------------------------------------------------
// Name: mouseFunc( )
// Desc: handles mouse stuff
//-----------------------------------------------------------------------------
void mouseFunc( int button, int state, int x, int y )
{
    if( button == GLUT_LEFT_BUTTON )
    {
        // when left mouse button is down
        if( state == GLUT_DOWN )
        {
        }
        else
        {
        }
    }
    else if ( button == GLUT_RIGHT_BUTTON )
    {
        // when right mouse button down
        if( state == GLUT_DOWN )
        {
        }
        else
        {
        }
    }
    else
    {
    }
    
    glutPostRedisplay( );
}




//-----------------------------------------------------------------------------
// Name: idleFunc( )
// Desc: callback from GLUT
//-----------------------------------------------------------------------------
void idleFunc( )
{
    // render the scene
    glutPostRedisplay( );
}


//-----------------------------------------------------------------------------
// Name: displayFunc( )
// Desc: callback function invoked to draw the client area
//-----------------------------------------------------------------------------
void displayFunc( )
{
    // clear the color and depth buffers
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    g_camera->computeFrame();
    g_camera->setCamera();
    
    // line width
    glLineWidth( 2.0 );
    // // step through and plot the waveform
    // GLfloat x = -5;
    // // increment
    // GLfloat xinc = ::fabs(2*x / (double) NUM_BUFFER_FRAMES);

    // glBegin( GL_LINE_STRIP );
    // for (int i = 0; i < NUM_BUFFER_FRAMES; i++) {
    //     glVertex2f(x + i * xinc, ::sin(i * TWOPI / (double) NUM_BUFFER_FRAMES));
    // }
    // glEnd();
    
    glColor3f(1.0, 1.0, 1.0);

    g_board->draw();

    for (int i = 0; i < Resident::allResidents.size(); i++) {
        Resident::allResidents[i]->computeFrame();
        Resident::allResidents[i]->draw();
    }

    // g_board->shuffleResidentsForStagedAreas();

    if (Keeper::theKeeper) Keeper::theKeeper->draw();
    if (Keeper::theKeeper) Keeper::theKeeper->computeFrame();


    g_camera->applyDarkness();

    NocturneMessage::computeFrame();

    // flush!
    glFlush( );
    // swap the double buffer
    glutSwapBuffers( );
}
