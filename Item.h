#ifndef ITEM_H
#define ITEM_H

#include <string>

enum ItemType {
	KEY,
	BOOK,
	PICTURE,
	TRINKET,
	WEAPON
};

class Item {
public:
	Item(std::string filename);

	std::string id;
	std::string name;
	ItemType item_type;
	std::string description;

private:

	// instance methods
	void readFile(std::string filename);
	void readID(std::string line);
	void readName(std::string line);
	void readType(std::string line);
	void readDescription(std::string line);
};

#endif