#ifndef AREA_H
#define AREA_H

#include "NoteLogic.h"
#include "Room.h"
#include "AreaSpecification.h"
#include "Quest.h"
#include "NocturneConstants.h"


#define MAX_CARPET_COLOR_COMPONENT 0.15


class Area {
public:

	// Area(int absolute_x, int absolute_y, AreaDefinition *definition);
	Area(int absolute_x, int absolute_y, AreaSpecification *specification);
	void createQuestNPCs(QuestSpecification *quest_spec);
	
	bool roomExists(int relative_x, int relative_y);
	Room *getRoom(int relative_x, int relative_y);

	Room *getLockedRoom();

	void lockRoom(int relative_x, int relative_y);
	void unlockRoom();

	int getX();
	int getY();

	NocturneAilment getAilment();

	void draw();
	bool checkQuest();
	bool questComplete();
	vector<string> getChatter();

private:

	// instance methods
	// Room *initRoom(int relative_x, int relative_y, AreaDefinition *definition);
	Room *initRoom(
		int relative_x, 
		int relative_y, 
		AreaSpecification *specification, 
		bool drop_resident_octave
	);
	
	void resetAreaWalls();
	void resetRoomWalls(int relative_x, int relative_y);

	Room *getRandomRoom();

	// instance variables
	int absolute_x, absolute_y;
	int num_rooms;

	Room *rooms[GRID_DIM_SIZE][GRID_DIM_SIZE];
	Room *quest_locked_room;

	float color[3];

	Quest* theQuest;
};

#endif