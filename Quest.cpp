#include "Quest.h"
#include "AssetManager.h"
#include "NocturneMessage.h"
#include <iostream>

using namespace std;

Quest::Quest(QuestSpecification* spec) {
	this->spec = spec;
	currentSegment = 0;
}

bool Quest::checkAdvance() {
	if (isComplete()) return false;
	if (spec->segments[currentSegment].isComplete()) {
		NocturneDialog* d = NocturneObject::assetManager->getDialog(spec->segments[currentSegment].completion_dialogue_id);
		if (d) d->play();
		currentSegment++;

		if (isComplete()) {
			for (int i = 0; i < spec->npc_stubs.size(); i++) {
				NocturneNPC::getNPC(spec->npc_stubs[i].id)->cure();
			}
		}
		return true;
	}
	return false;
}

bool Quest::shouldUnlockRoom() {
	// TODO: Handle quests with no unlock stage.
	if (!spec->requires_locked_room) {
		return true;
	}
	for (int i = 0; i < currentSegment; i++) {
		if (spec->segments[currentSegment].is_room_unlock_stage) return true;
	}
	return false;
}

bool Quest::isComplete() {
	return (currentSegment >= spec->segments.size());
}

NocturneAilment Quest::getAilment() {
	return spec->ailment_type;
}

vector<string> Quest::getChatter() {
	vector<string> toReturn;
	for (int i = 0; i < spec->general_chatter.size(); i++) {
		toReturn.push_back(spec->general_chatter[i]);
	}
	if (isComplete()) return toReturn;
	for (int i = 0; i < spec->segments[currentSegment].chatter.size(); i++) {
		toReturn.push_back(spec->segments[currentSegment].chatter[i]);
	}
	return toReturn;
}