
CXX=g++
INCLUDES=
FLAGS=-D__MACOSX_CORE__ -c
LIBS=-framework CoreAudio -framework CoreMIDI -framework CoreFoundation \
	-framework IOKit -framework Carbon  -framework OpenGL \
	-framework GLUT -framework Foundation \
	-framework AppKit -lstdc++ -lm \
	-L./ -lfluidsynth

VS_OBJS=   Timing.o RtAudio.o x-thread.o y-fluidsynth.o arcanum.o chuck_fft.o NoteLogic.o Board.o NocturneObject.o NocturnePerson.o Resident.o Keeper.o RandomGenerator.o Room.o Area.o AreaDefinition.o NocturneMessage.o NocturneCamera.o AreaSpecification.o Item.o NocturneNPC.o QuestSpecification.o Quest.o AssetManager.o

arcanum: $(VS_OBJS)
	$(CXX) -o arcanum $(VS_OBJS) $(LIBS)

arcanum.o: arcanum.cpp RtAudio.h chuck_fft.h NoteLogic.h Board.h y-fluidsynth.h NocturneConstants.h Resident.h Keeper.h AssetManager.h
	$(CXX) $(FLAGS) arcanum.cpp

AssetManager.o: AssetManager.h AssetManager.cpp NocturneMessage.h AreaSpecification.h Item.h QuestSpecification.h
	$(CXX) $(FLAGS) AssetManager.cpp

RtAudio.o: RtAudio.h RtAudio.cpp RtError.h
	$(CXX) $(FLAGS) RtAudio.cpp

chuck_fft.o: chuck_fft.h chuck_fft.c
	$(CXX) $(FLAGS) chuck_fft.c

NoteLogic.o: NoteLogic.h NoteLogic.cpp RandomGenerator.h
	$(CXX) $(FLAGS) NoteLogic.cpp

Board.o: Board.h Board.cpp RandomGenerator.h Resident.h Area.h
	$(CXX) $(FLAGS) Board.cpp

Area.o: Area.h Area.cpp Room.h AreaDefinition.h
	$(CXX) $(FLAGS) Area.cpp

AreaDefinition.o: AreaDefinition.h AreaDefinition.cpp RandomGenerator.h
	$(CXX) $(FLAGS) AreaDefinition.cpp

AreaSpecification.o: AreaSpecification.h AreaSpecification.cpp RandomGenerator.h
	$(CXX) $(FLAGS) AreaSpecification.cpp

QuestSpecification.o: QuestSpecification.h QuestSpecification.cpp
	$(CXX) $(FLAGS) QuestSpecification.cpp

Item.o: Item.h Item.cpp
	$(CXX) $(FLAGS) Item.cpp

Room.o: Room.h Room.cpp Resident.h
	$(CXX) $(FLAGS) Room.cpp

RandomGenerator.o: RandomGenerator.h RandomGenerator.cpp
	$(CXX) $(FLAGS) RandomGenerator.cpp

y-fluidsynth.o: y-fluidsynth.h y-fluidsynth.cpp x-thread.h
	$(CXX) $(FLAGS) y-fluidsynth.cpp

x-thread.o: x-thread.h x-thread.cpp x-def.h
	$(CXX) $(FLAGS) x-thread.cpp

Resident.o: Resident.h Resident.cpp NoteLogic.h y-fluidsynth.h NocturneConstants.h Timing.h NocturnePerson.h Keeper.h
	$(CXX) $(FLAGS) Resident.cpp

NocturneNPC.o: NocturneNPC.h NocturneNPC.cpp Resident.h
	$(CXX) $(FLAGS) NocturneNPC.cpp

Keeper.o: Keeper.h Keeper.cpp NocturnePerson.h Room.h NocturneMessage.h
	$(CXX) $(FLAGS) Keeper.cpp

Timing.o: Timing.h Timing.cpp
	$(CXX) $(FLAGS) Timing.cpp

NocturneObject.o: NocturneObject.h NocturneObject.cpp NocturnePerson.h Board.h Room.h Area.h AssetManager.h
	$(CXX) $(FLAGS) NocturneObject.cpp

NocturnePerson.o: NocturnePerson.h NocturnePerson.cpp NocturneConstants.h Timing.h NocturneObject.h
	$(CXX) $(FLAGS) NocturnePerson.cpp

NocturneCamera.o: NocturneCamera.h NocturneCamera.cpp NocturneObject.h Room.h Keeper.h
	$(CXX) $(FLAGS) NocturneCamera.cpp

NocturneMessage.o: NocturneMessage.h NocturneMessage.cpp Timing.h NocturnePerson.h NocturneNPC.h
	$(CXX) $(FLAGS) NocturneMessage.cpp

Quest.o: Quest.h Quest.cpp NocturneMessage.h
	$(CXX) $(FLAGS) Quest.cpp

clean:
	rm -f *~ *# *.o arcanum
