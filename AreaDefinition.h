#ifndef AREA_DEFINITION_H
#define AREA_DEFINITION_H

#include <vector>
#include <map>
#include <string>
#include "NoteLogic.h"
#include "Room.h"

#define GRID_DIM_SIZE 3

enum LineReadingMode { 
	NOT_AREA,
	AREA_FIRST_ROW, 
	AREA_SECOND_ROW, 
	AREA_THIRD_ROW, 
	AREA_SHUFFLE
};


class AreaDefinition {
public:
	static void initFromFile();
	static AreaDefinition *getAreaDefinition(int index);
	static AreaDefinition *getRandomAreaDefinition();
	
	static AreaDefinition *getRandomCompatibleAreaDefinition(
		Direction dir, 
		int prev_room_relative_x,
		int prev_room_relative_y
	);

	AreaDefinition();
	bool roomExists(int relative_x, int relative_y);
	bool canShuffle();
	struct Chord getInitialRoomChordDefinition(int area_relative_x, int area_relative_y);

private:

	// static methods
	static AreaDefinition *readLineFromFile(std::string line, LineReadingMode &lrm, AreaDefinition *current_area_definition);
	static AreaDefinition *processNonAreaLine(std::string line, LineReadingMode &lrm, AreaDefinition *current_area_definition);

	static bool areaCompatible(
		AreaDefinition *area_def, 
		Direction dir,
		int prev_area_def_room_relative_x,
		int prev_area_def_room_relative_y
	);
	static bool areaSidesCompatible(AreaDefinition *area_def_a, AreaDefinition *area_def_b, Direction dir);

	// instance methods
	LineReadingMode processAreaCellLine(std::string line, int cell_y);
	bool processAreaCellNotation(std::string cell_notation, int cell_x, int cell_y);
	// void shuffleNotesAcrossRooms(std::string line);
	void setShuffle(std::string line);

	// static variables
	static std::vector<AreaDefinition *> all_area_definitions;
	static std::map<std::string, struct Chord> all_chord_definitions;
	static std::map<std::string, struct ChordProgression> all_chord_progression_definitions;

	// instance variables
	bool room_exists[GRID_DIM_SIZE][GRID_DIM_SIZE];
	struct Chord initial_room_chord_definitions[GRID_DIM_SIZE][GRID_DIM_SIZE];
	// struct Chord current_room_chord_definitions[GRID_DIM_SIZE][GRID_DIM_SIZE];
	bool notes_shuffled_across_rooms;
	bool can_shuffle;

};

#endif