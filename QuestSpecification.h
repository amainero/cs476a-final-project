#ifndef QUEST_SPECIFICATION_H
#define QUEST_SPECIFICATION_H

#include "NoteLogic.h"
#include "NocturneNPC.h"
#include "NocturneConstants.h"

#include <string>
#include <vector>

struct NocturneNPCStub {
	string id;
	Note note;
	NocturneAilment ailment_type;
	int initial_relative_x, initial_relative_y;
	double npcColor[3];
};


struct NocturneNPCGroup {
	std::vector<std::string> npc_id_list;
};


enum QuestSegmentConditionType {
	IS_ALONE,
	IN_SAME_ROOM,
	IN_LOCKED_ROOM,
	HAS_ITEM
};


struct QuestSegmentCompletionCondition {
	bool isSatisfied();

	QuestSegmentConditionType condition_type;
	std::string npc_stub_id_1;
	std::string npc_stub_id_2;
	std::string item_stub_id;
};


struct QuestSegment {
	bool isComplete();

	std::string id;
	std::string completion_dialogue_id;
	std::vector<QuestSegmentCompletionCondition> conditions;
	std::vector<std::string> chatter;
	bool is_room_unlock_stage;
};


class QuestSpecification {
public:
	QuestSpecification(std::string filename);

	std::string id;
	NocturneQuestType quest_type;
	NocturneAilment ailment_type;
	std::vector<NocturneNPCStub> npc_stubs;
	std::vector<NocturneNPCGroup> npc_groups;

	std::vector<QuestSegment> segments;
	std::vector<std::string> general_chatter;
	bool requires_locked_room;

private:

	// instance methods
	void readFile(std::string filename);
	void readID(std::string line);
	void readQuestType(std::string line);
	void readAilment(std::string line);
	void readNPC(std::string line);
	void readNPCGroup(std::string line);
	void readGeneralChatter(std::string line);
	void readSegment(std::string line);
	void readRoomUnlockStage(std::string line);
	void readSegmentCompletionCondition(std::string line);
	void readSegmentChatter(std::string line, std::string &cur_segment_id);
};

#endif