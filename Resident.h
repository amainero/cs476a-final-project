#include "NocturnePerson.h"
#include "Keeper.h"

#include "NoteLogic.h"
#include "y-fluidsynth.h"
#include "RandomGenerator.h"
#include "NocturneConstants.h"
#include "Timing.h"
#include <vector>
#include <string>

#define NUM_CHANNELS 3
#define NUM_MIDI_NOTES 128

#define SING_DURATION 1.0
#define SING_FLUX 0.6
#define HUM_DURATION 2.0

#define SANE_MEAN_CHIRP_TIME 5.0
#define INSANE_MEAN_CHIRP_TIME 0.8
#define MAX_CHIRP_TIME_FLUX 0.7
#define SANE_MEAN_CHIRP_DURATION 0.5
#define INSANE_MEAN_CHIRP_DURATION 0.3
#define MAX_CHIRP_DURATION_FLUX 0.2

#define SANE_MEAN_PACE_TIME 10.0
#define INSANE_MEAN_PACE_TIME 2.0
#define PACE_FLUX 1.0
#define SANE_PACE_DIST (4.0 * PERSON_RADIUS)
#define INSANE_PACE_DIST (10.0 * PERSON_RADIUS)
#define SANE_PERSONAL_SPACE (5.0 * PERSON_RADIUS)
#define INSANE_PERSONAL_SPACE (2.0 * PERSON_RADIUS)
#define PERSONAL_SPACE_MARGIN_OF_ERROR (0.25 * PERSON_RADIUS)

#define MAX_SING_SIZE (PERSON_RADIUS * 1.4)
#define CHIRP_SIZE (PERSON_RADIUS * 1.2)
#define SHRINK_SPEED (PERSON_RADIUS * 0.4)

#define FOLLOW_DIST (5.0 * PERSON_RADIUS)
#define FOLLOW_INDICATOR_RADIUS (PERSON_RADIUS / 2)

#define CHATTER_TIME 3.0
#define CHATTER_MEAN_PAUSE_TIME 40.0
#define CHATTER_FLUX_PAUSE_TIME 20.0

#ifndef RESIDENT_H
#define RESIDENT_H

using namespace std;

class Resident;

enum ResidentEventType { CHIRP_EVENT, SING_EVENT, HUM_EVENT, UNKNOWN_EVENT };

struct ResidentEvent {
	int channel;
	Resident* resident;
	int velocity;
	bool noteOn;
	ResidentEventType eType;
	double fireTime;
};

class Resident: public NocturnePerson {
public:
	static vector<Resident*> allResidents;

	Resident();
	~Resident();

	Note note;
	/*void setRoomRelativeX(double x);
	void setRoomRelativeY(double y);
	double getRoomRelativeX();
	double getRoomRelativeY();*/

	static void loadResidentSynth();
	static void processEvents();
	static void synthesize(SAMPLE* outputBuffer, int numFrames);

	static bool compare(Resident *resident_a, Resident *resident_b);

	virtual bool isNPC() { return false; }

	void sing();
	void hum();
	void goSilent();
	void startChirping();
	void stopChirping();

	void followKeeper();
	void stopFollowingKeeper();
	bool currentlyFollowingKeeper();
	bool currentlyCanChangeRooms();

	void highlight();
	void unhighlight();
	bool currentlyHighlighted();

	void draw();

private:
	static YFluidSynth* residentSynth; // Might we need to give each resident its own synth?
	static int noteOnStack[NUM_CHANNELS][NUM_MIDI_NOTES];
	static vector<ResidentEvent> events;

	void play(int channel, int pitch, int velocity, double duration);
	void scheduleChirp();
	void prepareChirp();
	double chirpTime();
	double chirpDuration();

	double paceTime();
	void resetPaceClock();
	void pace();
	void paceLocation(double newLoc[2]);
	double personalSpace();
	void resolvePersonalSpace(NocturnePerson *otherPerson);

	void putDistanceBetween(NocturneObject *otherThing, double distance);
	void moveTowardsKeeper();
	virtual void computeBehavior(double timeLapsed);
	virtual void collisionBehavior(NocturnePerson* otherPerson, double collisionStrength);
	virtual void onRoomChange(Room *newRoom);

	string getNextQuote();
	void scheduleChatter();
	void chatter();


	int chirpChannel;
	int singChannel;
	int humChannel;
	bool isChirping;
	bool isSinging;
	bool isHumming;
	bool isFollowingKeeper;
	bool chirpScheduled;
	bool isHighlighted;

	double nextPaceTime;

	string currentChatter;
	double prevChatterTime;
	double nextChatterTime;
};

#endif