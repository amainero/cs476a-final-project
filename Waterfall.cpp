#include "Waterfall.h"
using namespace std;


void Waterfall::pushBuffer(BufferWrapper buffer) {
	// note: this passes in the buffer by copy
	waterfall_buffers.push_front(buffer);
	if (waterfall_buffers.size() > NUM_BUFFERS_IN_WATERFALL) {
		waterfall_buffers.pop_back();
	}
}


deque<BufferWrapper> Waterfall::getAllBuffers() {
	return waterfall_buffers;
}