#include "NocturnePerson.h"
#include "NocturneConstants.h"

#ifndef KEEPER_H
#define KEEPER_H

#define KEEPER_FREE_MOVEMENT true
#define KEEPER_COLLISIONS (KEEPER_FREE_MOVEMENT && true)
#define KEEPER_MOVE_COMMAND_TIME 1.5

#define KEEPER_SANITY_LOSS 0.05
#define KEEPER_SANITY_REWARD 0.7

class Resident;
class NocturneDialog;

enum KeeperDirection { UP, DOWN, LEFT, RIGHT };

class Keeper: public NocturnePerson {
public:
	Keeper();

	static void keyboardCallback( unsigned char key, int x, int y );
	static void keyboardUpCallback( unsigned char key, int x, int y );

	static Keeper* theKeeper;

	static NocturneDialog* spaaace;
private:

	bool dirDown[4];

	virtual void computeBehavior(double timeLapsed);
	virtual void onRoomChange(Room *newRoom);

	void kCall(unsigned char key, int x, int y);
	void kUpCall(unsigned char key, int x, int y);

	void acquireFollower();
	void releaseFollower();

	Resident *follower;

};

#endif