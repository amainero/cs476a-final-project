#ifndef RANDOM_GEN_H
#define RANDOM_GEN_H

class RandomGenerator {
public:
	static double randomFloat();
	static int randomInt(int min, int max);
};


#endif