#ifndef ASSET_MANAGER_H
#define ASSET_MANAGER_H

#include "NocturneMessage.h"
#include "QuestSpecification.h"
#include "Item.h"
#include "NoteLogic.h"
#include "AreaSpecification.h"

#include <string>
#include <map>

class AssetManager {
public:
	AssetManager();

	// basic getters
	NocturneDialog *getDialog(std::string dialog_id);
	Item *getItem(std::string item_id);
	AreaSpecification *getAreaSpecification(std::string area_spec_id);
	QuestSpecification *getQuestSpecification(std::string quest_spec_id);
	Chord getChord(std::string chord_id);

	AreaSpecification *getRandomAreaSpecification();

	// for area generation
	AreaSpecification *getCompatibleAreaSpecification(
		Direction dir,
		int prev_room_relative_x,
		int prev_room_relative_y
	);

	QuestSpecification *getCompatibleQuestSpecification(
		AreaSpecification *area_specification
	);

	bool areaCompatible(
		AreaSpecification *area_spec,
		Direction dir,
		int prev_area_def_room_relative_x,
		int prev_area_def_room_relative_y
	);

	bool questCompatible(
		AreaSpecification *area_spec, 
		QuestSpecification *quest_spec
	);

	// this is a general tokenizer tool, should probably be in a different class
	static std::vector<std::string> &split(
		const std::string &s, 
		std::vector<std::string> &elems,
		char delimiter = ' '
	);


private:

	// instance methods
	void readChordFile(std::string filename);

	// instance variables
	std::map<std::string, NocturneDialog *> dialog_assets;
	std::map<std::string, Item *> item_assets;
	std::map<std::string, AreaSpecification *> area_spec_assets;
	std::map<std::string, QuestSpecification *> quest_spec_assets;
	std::map<std::string, Chord> chord_assets;

	std::vector<std::string> used_quest_spec_ids;
};

#endif