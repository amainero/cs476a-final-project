#ifdef __MACOSX_CORE__
  // note: for mac only
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
  #include <GL/glut.h>
#endif

#ifndef BOARD_H
#define BOARD_H

#include "RandomGenerator.h"
#include "Resident.h"
#include "Area.h"


class Board {
public:
	Board();

	int getGridCoordinateFromRawCoordinateX(GLfloat absolute_x);
	int getGridCoordinateFromRawCoordinateY(GLfloat absolute_y);

	Room *getRoomFromGridCoordinates(int absolute_x, int absolute_y);
	Room *getRoomFromRawCoordinates(GLfloat absolute_x, GLfloat absolute_y);

	Area *getAreaFromGridCoordinates(int absolute_x, int absolute_y);
	Area *getAreaFromRawCoordinates(GLfloat absolute_x, GLfloat absolute_y);

	Area *constructArea(Room *prev_room, Direction dir);
	std::vector<Area *> getAllAreas();

	void draw();

private:

	void resetAreaBorderWalls(Area *area);
	void resetAreaBorderWall(Area *area, Direction dir);

	void updateRoomsForMovingResidents(Room *room);

	int min_area_x, min_area_y;
	int area_x_range, area_y_range;
	std::vector<Area *> areas;
};

#endif