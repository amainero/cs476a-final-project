#ifdef __MACOSX_CORE__
  // note: for mac only
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
  #include <GL/glut.h>
#endif

#include "Area.h"
#include "NocturneObject.h"
#include "AssetManager.h"
#include <iostream>
using namespace std;

#define RESIDENT_BASS_NOTE_PROB 0.5

Area::Area(int absolute_x, int absolute_y, AreaSpecification *specification) {
	this->absolute_x = absolute_x;
	this->absolute_y = absolute_y;
	this->num_rooms = specification->num_rooms;
	this->quest_locked_room = NULL;

	bool drop_resident_octave = RandomGenerator::randomFloat() < RESIDENT_BASS_NOTE_PROB;

	// create all rooms from area definitions
	for (int i = 0; i < GRID_DIM_SIZE; i++) {
		for (int j = 0; j < GRID_DIM_SIZE; j++) {
			if (specification->roomExists(i, j)) {
				Room *next_room = initRoom(i, j, specification, drop_resident_octave);
				rooms[i][j] = next_room;
			} else {
				rooms[i][j] = NULL;
			}
		}
	}

	if (specification->locked_room_x != -1) {
		lockRoom(specification->locked_room_x, specification->locked_room_y);
	}
	resetAreaWalls();

	color[0] = RandomGenerator::randomFloat() * MAX_CARPET_COLOR_COMPONENT;
    color[1] = RandomGenerator::randomFloat() * MAX_CARPET_COLOR_COMPONENT;
    color[2] = RandomGenerator::randomFloat() * MAX_CARPET_COLOR_COMPONENT;

    QuestSpecification* quest_spec = NocturneObject::assetManager->getCompatibleQuestSpecification(specification);

    if (quest_spec) {
    	theQuest = new Quest(quest_spec);
    } else {
    	theQuest = NULL;
    }

    if (theQuest && theQuest->shouldUnlockRoom()) {
    	unlockRoom();
    } else if (!theQuest) {
    	unlockRoom();
    }

    if (theQuest) {
    	createQuestNPCs(quest_spec);
	}
}


void Area::createQuestNPCs(QuestSpecification *quest_spec) {
	// create residents NPCs for each NPC stub
	vector<NocturneNPC *> residentNPCs;
	for (int i = 0; i < quest_spec->npc_stubs.size(); i++) {
		NocturneNPCStub npc_stub = quest_spec->npc_stubs[i];
		NocturneNPC *residentNPC = new NocturneNPC(npc_stub.id, npc_stub.ailment_type, npc_stub.npcColor);
		residentNPC->note = npc_stub.note;
		residentNPCs.push_back(residentNPC);
	}
	// add residents to room
	vector<NocturneNPC *> addedResidentNPCs;
	vector<Room *> addedRooms;

	for (int group_index = 0; group_index < quest_spec->npc_groups.size(); group_index++) {
		NocturneNPCGroup npc_group = quest_spec->npc_groups[group_index];
		// choose a room for them
		while (true) {
			Room *random_room = getRandomRoom();
			bool room_already_chosen = false;

			// check if room already chosen
			for (int i = 0; i < addedRooms.size(); i++) {
				if ((addedRooms[i] == random_room) && (num_rooms - 1 > quest_spec->npc_groups.size())) {
					// try again
					room_already_chosen = true;
				}
			}
			if (room_already_chosen) {
				continue;
			}

			if (random_room == NULL) {
				cout << "Error: random room null in NPC loading" << endl;
				continue;
			}

			// room not already chosen, so we add residents to the room
			for (int i = 0; i < npc_group.npc_id_list.size(); i++) {
				// get resident
				NocturneNPC *residentNPC = NULL;
				for (int j = 0; j < residentNPCs.size(); j++) {
					if (npc_group.npc_id_list[i] == residentNPCs[j]->getID()) {
						residentNPC = residentNPCs[j];
						break;
					}
				}

				if (!residentNPC) {
					cout << "Error: resident NPC doesn't exist!" << npc_group.npc_id_list[i] << endl;
					break;
				}

				// cout << residentNPC->getID() << ": random room: " << random_room->getRelativeX() << " " << random_room->getRelativeY() << endl;
				random_room->setResidentRandomLocation(residentNPC);
			}
			// add room to list of chosen rooms
			addedRooms.push_back(random_room);

			// success
			break;
		}

	}
	// possibly: keep track of them somewhere
}


Room *Area::getRoom(int relative_x, int relative_y) {
	// will return NULL if that room doesn't exist
	return rooms[relative_x][relative_y];
}


// true if room not NULL
bool Area::roomExists(int relative_x, int relative_y) {
	return rooms[relative_x][relative_y];
}


NocturneAilment Area::getAilment() {
	if (!theQuest) return NO_AILMENT;
	else return theQuest->getAilment();
}


Room *Area::initRoom(int relative_x, int relative_y, AreaSpecification *specification, bool drop_resident_octave) {
	Room *next_room = new Room(absolute_x, absolute_y, relative_x, relative_y);
	// TODO: real wall logic
	next_room->setWall(NORTH, false);
	next_room->setWall(SOUTH, false);
	next_room->setWall(EAST, false);
	next_room->setWall(WEST, false);
	struct Chord room_initial_notes = specification->getInitialRoomChordDefinition(relative_x, relative_y);

	if (drop_resident_octave) {
		for (int i = 0; i < room_initial_notes.notes.size(); i++) {
			room_initial_notes.notes[i].semitone -= 12;
		}
	}

	next_room->createInitialResidentsFromNotes(room_initial_notes.notes);
	return next_room;
}


bool Area::checkQuest() {
	if (!theQuest) return false;
	if (theQuest->checkAdvance()) {
		if (theQuest->shouldUnlockRoom()) unlockRoom();
		return true;
	}
	return false;
}

bool Area::questComplete() {
	if (!theQuest) return true;
	return theQuest->isComplete();
}

vector<string> Area::getChatter() {
	vector<string> toReturn;
	if (!theQuest) return toReturn;
	else return theQuest->getChatter();
}


void Area::draw() {
	for (int i = 0; i < GRID_DIM_SIZE; i++) {
		for (int j = 0; j < GRID_DIM_SIZE; j++) {
			if (rooms[i][j] != NULL) {
				// TODO: Determine what we want to do with the carpet.
				//rooms[i][j]->draw(color);
				float grey[3] = {0.15,0.15,0.15};
				rooms[i][j]->draw(grey);
			}
		}
	}

	if (quest_locked_room) {
		quest_locked_room->drawLockIndicators();
	}
}


int Area::getX() {
	return this->absolute_x;
}


int Area::getY() {
	return this->absolute_y;
}


void Area::lockRoom(int relative_x, int relative_y) {
	// TODO: make this work across areas
	if (relative_x < 0 || relative_y < 0) return;
	quest_locked_room = rooms[relative_x][relative_y];
	quest_locked_room->setLocked(true);
	resetRoomWalls(relative_x, relative_y);
}


void Area::unlockRoom() {
	quest_locked_room->setLocked(false);
	resetAreaWalls();
}


Room *Area::getLockedRoom() {
	return quest_locked_room;
}

Room *Area::getRandomRoom() {
	int random_room_index = RandomGenerator::randomInt(0, num_rooms - 2);

	int current_room_index = 0;
	for (int i = 0; i < GRID_DIM_SIZE; i++) {
		for (int j = 0; j < GRID_DIM_SIZE; j++) {

			if (!roomExists(i,j)) {
				continue;
			}

			Room *current_room = getRoom(i, j);
			if (getLockedRoom() == current_room) {
				continue;
			}

			if (current_room_index == random_room_index) {
				return current_room;
			} else {
				current_room_index++;
			}
		}
	}
	return NULL;
}

void Area::resetAreaWalls() {
	for (int i = 0; i < GRID_DIM_SIZE; i++) {
		for (int j = 0; j < GRID_DIM_SIZE; j++) {
			if (roomExists(i, j)) {
				resetRoomWalls(i, j);
			}
		}
	}
}


void Area::resetRoomWalls(int relative_x, int relative_y) {
	Room *room = getRoom(relative_x, relative_y);
	// check north room
	int other_room_relative_x = relative_x;
	int other_room_relative_y = relative_y + 1;
	if (other_room_relative_y < 3 && roomExists(other_room_relative_x, other_room_relative_y)) {
		Room *other_room = getRoom(other_room_relative_x, other_room_relative_y);
		if ((!room->isLocked()) && (!other_room->isLocked())) {
			room->setWall(NORTH, true);
		}
	}
	// check south room
	other_room_relative_y = relative_y - 1;
	if (other_room_relative_y >= 0 && roomExists(other_room_relative_x, other_room_relative_y)) {
		Room *other_room = getRoom(other_room_relative_x, other_room_relative_y);
		if ((!room->isLocked()) && (!other_room->isLocked())) {
			room->setWall(SOUTH, true);

		}
	}
	// check west room
	other_room_relative_y = relative_y;
	other_room_relative_x = relative_x - 1;
	if (other_room_relative_x >= 0 && roomExists(other_room_relative_x, other_room_relative_y)) {
		Room *other_room = getRoom(other_room_relative_x, other_room_relative_y);
		if ((!room->isLocked()) && (!other_room->isLocked())) {
			room->setWall(WEST, true);
		}
	}
	// check east room
	other_room_relative_x = relative_x + 1;
	if (other_room_relative_x < 3 && roomExists(other_room_relative_x, other_room_relative_y)) {
		Room *other_room = getRoom(other_room_relative_x, other_room_relative_y);
		if ((!room->isLocked()) && (!other_room->isLocked())) {
			room->setWall(EAST, true);
		}
	}

}
