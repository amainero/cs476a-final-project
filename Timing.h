#ifndef TIMING_H
#define TIMING_H

#include <sys/time.h>

// In seconds since epoch
double currentTime();

#endif