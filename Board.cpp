#include "Board.h"
#include "AssetManager.h"
#include <iostream>
using namespace std;


Board::Board() {

	AreaSpecification *first_area_specification = NocturneObject::assetManager->getRandomAreaSpecification();
	Area *first_area = new Area(0, 0, first_area_specification);

	min_area_x = min_area_y = 0;
	area_x_range = area_y_range = GRID_DIM_SIZE;
	areas.push_back(first_area);
	resetAreaBorderWalls(first_area);
}


Area *Board::constructArea(Room *prev_room, Direction dir) {

	// get coordinates for old area
	int prev_room_absolute_x = prev_room->getAbsoluteX();
	int prev_room_absolute_y = prev_room->getAbsoluteY();

	int prev_room_relative_x = prev_room->getRelativeX();
	int prev_room_relative_y = prev_room->getRelativeY();

	// init coordinates for new area
	int prev_area_x = prev_room_absolute_x - prev_room_relative_x;
	int prev_area_y = prev_room_absolute_y - prev_room_relative_y;

	int new_area_x = prev_area_x;
	int new_area_y = prev_area_y;

	// calculate coordinates for new area based on direction
	switch (dir) {
		case NORTH:
			new_area_y += GRID_DIM_SIZE;
			area_y_range += GRID_DIM_SIZE;
			break;
		case SOUTH:
			new_area_y -= GRID_DIM_SIZE;
			area_y_range += GRID_DIM_SIZE;
			break;
		case EAST:
			new_area_x += GRID_DIM_SIZE;
			area_x_range += GRID_DIM_SIZE;
			break;
		case WEST:
			new_area_x -= GRID_DIM_SIZE;
			area_x_range += GRID_DIM_SIZE;
			break;
	}

	// get an area definition and init a new area
	// AreaDefinition *next_area_definition = AreaDefinition::getRandomCompatibleAreaDefinition(
	// 	dir,
	// 	prev_room_relative_x, 
	// 	prev_room_relative_y
	// );
	// Area *new_area = new Area(new_area_x, new_area_y, next_area_definition);

	AreaSpecification *new_area_specification = NocturneObject::assetManager->getCompatibleAreaSpecification(
		dir,
		prev_room_relative_x,
		prev_room_relative_y
	);
	Area *new_area = new Area(new_area_x, new_area_y, new_area_specification);

	// update ranges
	if (new_area_x < min_area_x) {
		min_area_x = new_area_x;
	}
	if (new_area_y < min_area_y) {
		min_area_y = new_area_y;
	}
	// update area list
	areas.push_back(new_area);

	// close off all now incompatible doors to nonexistent rooms
	resetAreaBorderWalls(new_area);
	Area *north_area = getAreaFromGridCoordinates(new_area_x, new_area_y + GRID_DIM_SIZE);
	Area *south_area = getAreaFromGridCoordinates(new_area_x, new_area_y - GRID_DIM_SIZE);
	Area *east_area = getAreaFromGridCoordinates(new_area_x + GRID_DIM_SIZE, new_area_y);
	Area *west_area = getAreaFromGridCoordinates(new_area_x - GRID_DIM_SIZE, new_area_y);

	if (north_area) {
		resetAreaBorderWall(north_area, SOUTH);
	}
	if (south_area) {
		resetAreaBorderWall(south_area, NORTH);
	}
	if (east_area) {
		resetAreaBorderWall(east_area, WEST);
	}
	if (west_area) {
		resetAreaBorderWall(west_area, EAST);
	}
	return new_area;
}


int Board::getGridCoordinateFromRawCoordinateX(GLfloat absolute_x) {
	GLfloat grid_absolute_x = (absolute_x + ROOM_DRAW_OFFSET_X) / ROOM_SIZE;
	return floor(grid_absolute_x);
}


int Board::getGridCoordinateFromRawCoordinateY(GLfloat absolute_y) {
	GLfloat grid_absolute_y = (absolute_y + ROOM_DRAW_OFFSET_Y) / ROOM_SIZE;
	return ceil(grid_absolute_y); // invert y-coordinates
}



Room *Board::getRoomFromGridCoordinates(int absolute_x, int absolute_y) {
	// round out board coordinates
	Area *area = getAreaFromGridCoordinates(absolute_x, absolute_y);
	if (!area) {
		return NULL;
	}

	int room_relative_x = absolute_x - area->getX();
	int room_relative_y = absolute_y - area->getY();

	if (area->roomExists(room_relative_x, room_relative_y)) {
		return area->getRoom(room_relative_x, room_relative_y);
	}
	return NULL;
}


Room *Board::getRoomFromRawCoordinates(GLfloat absolute_x, GLfloat absolute_y) {
	// TODO: make offset functionality more modular (put a static function in NocturneObject?)
	int grid_absolute_x = getGridCoordinateFromRawCoordinateX(absolute_x);
	int grid_absolute_y = getGridCoordinateFromRawCoordinateY(absolute_y);
	return getRoomFromGridCoordinates(grid_absolute_x, grid_absolute_y);
}


Area *Board::getAreaFromGridCoordinates(int absolute_x, int absolute_y) {

	int area_x = absolute_x;
	int area_y = absolute_y;

	int room_relative_x = 0;
	int room_relative_y = 0;

	while (area_x % GRID_DIM_SIZE != 0) {
		area_x -= 1;
		room_relative_x += 1;
	}
	while (area_y % GRID_DIM_SIZE != 0) {
		area_y -= 1;
		room_relative_y += 1;
	}

	for (int i = 0; i < areas.size(); i++) {
		Area *area = areas[i];
		if (area->getX() == area_x && area->getY() == area_y) {
			return area;
		}
	}
	return NULL;
}


Area *Board::getAreaFromRawCoordinates(GLfloat absolute_x, GLfloat absolute_y) {
	int grid_absolute_x = getGridCoordinateFromRawCoordinateX(absolute_x);
	int grid_absolute_y = getGridCoordinateFromRawCoordinateX(absolute_y);
	return getAreaFromGridCoordinates(grid_absolute_x, grid_absolute_y);
}


void Board::resetAreaBorderWalls(Area *area) {
	// this is to create wall connectivity between areas
	resetAreaBorderWall(area, NORTH);
	resetAreaBorderWall(area, SOUTH);
	resetAreaBorderWall(area, WEST);
	resetAreaBorderWall(area, EAST);
}


void Board::resetAreaBorderWall(Area *area, Direction dir) {
	int area_x = area->getX();
	int area_y = area->getY();

	// initialize room coordinates to be modified based on direction
	int room_absolute_x;
	int room_absolute_y;

	// initialize other room coordinates to be modified based on direction
	int other_room_absolute_x;
	int other_room_absolute_y;

	Direction opposite_dir;

	switch (dir) {
		case NORTH:
			room_absolute_y = area_y + GRID_DIM_SIZE - 1;
			other_room_absolute_y = area_y + GRID_DIM_SIZE;
			opposite_dir = SOUTH;
			break;
		case SOUTH:
			room_absolute_y = area_y;
			other_room_absolute_y = area_y - 1;
			opposite_dir = NORTH;
			break;
		case EAST:
			room_absolute_x = area_x + GRID_DIM_SIZE - 1;
			other_room_absolute_x = area_x + GRID_DIM_SIZE;
			opposite_dir = WEST;
			break;
		case WEST:
			room_absolute_x = area_x;
			other_room_absolute_x = area_x - 1;
			opposite_dir = EAST;
			break;
	}

	// run the length of the border
	for (int i = 0; i < GRID_DIM_SIZE; i++) {
		switch (dir) {
			case NORTH:
			case SOUTH:
				room_absolute_x = other_room_absolute_x = area_x + i;
				break;
			case EAST:
			case WEST:
				room_absolute_y = other_room_absolute_y = area_y + i;
				break;
		}

		Room *room = getRoomFromGridCoordinates(room_absolute_x, room_absolute_y);
		if (!room) {
			// only modify walls for rooms that exist in our given area
			continue;
		}
		
		Area *other_area = getAreaFromGridCoordinates(other_room_absolute_x, other_room_absolute_y);
		Room *other_room = getRoomFromGridCoordinates(other_room_absolute_x, other_room_absolute_y);
		if (!other_area) {
			// area not yet added: open space
			room->setWall(dir, true);
		} else if (other_room && !other_room->isLocked() && !room->isLocked()) {
			// area added and a connecting room exists
			room->setWall(dir, true);
			other_room->setWall(opposite_dir, true);
		} else if (other_room && (other_room->isLocked() || room->isLocked())) {
			room->setWall(dir, false);
			other_room->setWall(opposite_dir, false);
		} else {
			// other area exists, but a room does not
			room->setWall(dir, false);
		}
	}
}


void Board::draw() {
	for (int i = 0; i < areas.size(); i++) {
		areas[i]->draw();
	}
}