#include "Resident.h"
#include "Keeper.h"
#include "NocturneMessage.h"
#include "Area.h"
#include <iostream>

// TODO: How do we associate residents with rooms?

using namespace std;

YFluidSynth* Resident::residentSynth = new YFluidSynth();
vector<ResidentEvent> Resident::events = vector<ResidentEvent>();
vector<Resident*> Resident::allResidents = vector<Resident*>();
int Resident::noteOnStack[NUM_CHANNELS][NUM_MIDI_NOTES];

Resident::Resident() {
	sanity = RandomGenerator::randomFloat();
	isChirping = false;
	chirpChannel = 2;
	isSinging = false;
	singChannel = 0;
	isHumming = false;
	humChannel = 1;
	isFollowingKeeper = false;
	chirpScheduled = false;
	nextPaceTime = currentTime() + paceTime();
	scheduleChatter();
	nextChatterTime -= 20.0;
	Resident::allResidents.push_back(this);
}

Resident::~Resident() {
	Resident::allResidents.erase(
		remove(Resident::allResidents.begin(), Resident::allResidents.end(), this),
		Resident::allResidents.end()
	);
}


bool Resident::compare(Resident *resident_a, Resident *resident_b) {
	return resident_b->note.semitone < resident_a->note.semitone;
}


void Resident::loadResidentSynth() {
	Resident::residentSynth->init(AUDIO_SAMPLE_RATE, 100);
	Resident::residentSynth->load("./SoundFonts/arcanumVoices.sf2", "sf2");
	Resident::residentSynth->programChange(0, 0);
	Resident::residentSynth->programChange(1, 1);
	Resident::residentSynth->programChange(2, 2);

	for (int i = 0; i < NUM_CHANNELS; i++) {
		for (int j = 0; j < NUM_MIDI_NOTES; j++) {
			Resident::noteOnStack[i][j] = 0;
		}
	}
}


void Resident::processEvents() {
	double theTime = currentTime();
	for (int i = 0; i < Resident::events.size(); i++) {
		if (Resident::events[i].fireTime <= theTime) {
			
			// FIRE!!!
			bool fireAway = true;
			ResidentEvent firing = Resident::events[i];

			// Chirp behavior.
			if (firing.eType == CHIRP_EVENT && firing.noteOn) {
				firing.resident->prepareChirp();
				if (!firing.resident->isChirping || firing.resident->isSinging) fireAway = false;
			}

			// Sing behavior.
			if (firing.eType == SING_EVENT && firing.noteOn) firing.resident->isSinging = true;
			if (firing.eType == SING_EVENT && !firing.noteOn) {
				firing.resident->isSinging = false;
				firing.resident->radius = PERSON_RADIUS;
			}

			// Hum behavior.
			if (firing.eType == HUM_EVENT && firing.noteOn) firing.resident->isHumming = true;
			if (firing.eType == HUM_EVENT && !firing.noteOn) firing.resident->isHumming = false;

			// Fire the event.
			if (fireAway) {
				int pitch = NoteLogic::getMIDIKeyForNote(firing.resident->note);
				if (firing.noteOn) {
					Resident::residentSynth->noteOn(firing.channel, pitch, firing.velocity);
					Resident::noteOnStack[firing.channel][pitch]++;
				}
				else if (--Resident::noteOnStack[firing.channel][pitch] <= 0) {
					Resident::noteOnStack[firing.channel][pitch] = 0;
					Resident::residentSynth->noteOff(firing.channel, pitch);
				}
			}

			// Erase the event.
			Resident::events.erase(Resident::events.begin() + i);
			i--;
		}
	}
}


void Resident::synthesize(SAMPLE* outputBuffer, int numFrames) {
	Resident::residentSynth->synthesize2(outputBuffer, numFrames);
}


void Resident::play(int channel, int pitch, int velocity, double duration) {
	Resident::residentSynth->noteOff(channel, pitch);
	Resident::residentSynth->noteOn(channel, pitch, velocity);
	Resident::noteOnStack[channel][pitch]++;

	ResidentEvent e;
	e.channel = channel;
	e.resident = this;
	e.noteOn = false;
	if (channel == singChannel) {
		e.eType = SING_EVENT;
		isSinging = true;
	}
	else if (channel == humChannel) {
		e.eType = HUM_EVENT;
		isHumming = true;
	}
	else e.eType = UNKNOWN_EVENT;
	e.fireTime = currentTime() + duration;

	Resident::events.push_back(e);
}


void Resident::sing() {
	goSilent();
	double duration = SING_DURATION - SING_FLUX + (2.0 * SING_FLUX * RandomGenerator::randomFloat());
	play(singChannel, NoteLogic::getMIDIKeyForNote(note), 127, duration);
}


void Resident::hum() {
	goSilent();
	play(humChannel, NoteLogic::getMIDIKeyForNote(note), 127, HUM_DURATION);
}

void Resident::goSilent() {
	int pitch = NoteLogic::getMIDIKeyForNote(note);
	isSinging = false;
	isHumming = false;
	Resident::residentSynth->noteOff(singChannel, pitch);
	Resident::residentSynth->noteOff(humChannel, pitch);
	Resident::residentSynth->noteOff(chirpChannel, pitch);
}

void Resident::startChirping() {
	if (!isChirping) scheduleChirp();
	isChirping = true;
}


void Resident::stopChirping() {
	isChirping = false; // Possible bug if you start chirping again before the last chirp event passes. (You'd get double chirping.)
}

void Resident::prepareChirp() {
	chirpScheduled = false;
	if (isChirping) {
		scheduleChirp();

		// Turn green...
		//color[0] = 0.0;
		//color[2] = 0.0;

		// Get bigger
		radius = CHIRP_SIZE;
	}
}


void Resident::scheduleChirp() {
	if (chirpScheduled) return;
	chirpScheduled = true;

	ResidentEvent chirpOn;
	chirpOn.channel = chirpChannel; // Could we choose between different chirps?
	chirpOn.resident = this;
	chirpOn.velocity = 127;
	chirpOn.noteOn = true;
	chirpOn.eType = CHIRP_EVENT;
	chirpOn.fireTime = currentTime() + chirpTime();

	ResidentEvent chirpOff;
	chirpOff.channel = chirpChannel;
	chirpOff.resident = this;
	chirpOff.noteOn = false;
	chirpOff.eType = CHIRP_EVENT;
	chirpOff.fireTime = chirpOn.fireTime + chirpDuration();

	Resident::events.push_back(chirpOn);
	Resident::events.push_back(chirpOff);
}


double Resident::chirpTime() {
	double meanChirpTime = INSANE_MEAN_CHIRP_TIME + (SANE_MEAN_CHIRP_TIME - INSANE_MEAN_CHIRP_TIME) * sanity;
	double chirpTimeFlux = MAX_CHIRP_TIME_FLUX * (1.0 - sanity);
	return (meanChirpTime - chirpTimeFlux) + (2.0 * chirpTimeFlux) * RandomGenerator::randomFloat();
}


double Resident::chirpDuration() {
	double meanChirpDuration = INSANE_MEAN_CHIRP_DURATION + (SANE_MEAN_CHIRP_DURATION - INSANE_MEAN_CHIRP_DURATION) * sanity;
	double chirpDurationFlux = MAX_CHIRP_DURATION_FLUX * (1.0 - sanity);
	return (meanChirpDuration - chirpDurationFlux) + (2.0 * chirpDurationFlux) * RandomGenerator::randomFloat();
}

double Resident::paceTime() {
	double meanPaceTime = INSANE_MEAN_PACE_TIME + (SANE_MEAN_PACE_TIME - INSANE_MEAN_PACE_TIME) * sanity;
	double toReturn = (meanPaceTime - PACE_FLUX) + (2.0 * PACE_FLUX) * RandomGenerator::randomFloat();
	if (toReturn < 0.0) toReturn = 0.0;
	return toReturn;
}

void Resident::paceLocation(double newLoc[2]) {
	double paceDist = INSANE_PACE_DIST + (SANE_PACE_DIST - INSANE_PACE_DIST) * sanity;
	double angle = TWOPI * RandomGenerator::randomFloat();
	newLoc[0] = x_loc + cos(angle) * paceDist;
	newLoc[1] = y_loc + sin(angle) * paceDist;

	// Make sure we don't run into a wall...if we're sane enough.
	double northY = floor((y_loc + ROOM_OFFSET) / ROOM_SIZE) * ROOM_SIZE + ROOM_OFFSET;
	double southY = (floor((y_loc + ROOM_OFFSET) / ROOM_SIZE) - 1) * ROOM_SIZE + ROOM_OFFSET;
	double eastX = floor((x_loc + ROOM_OFFSET) / ROOM_SIZE) * ROOM_SIZE + ROOM_OFFSET;
	double westX = (floor((x_loc + ROOM_OFFSET) / ROOM_SIZE) - 1) * ROOM_SIZE + ROOM_OFFSET;
	newLoc[0] = max(newLoc[0], westX + personalSpace());
	newLoc[0] = min(newLoc[0], eastX - personalSpace());
	newLoc[1] = max(newLoc[1], southY + personalSpace());
	newLoc[1] = min(newLoc[1], northY - personalSpace());
}

void Resident::resetPaceClock() {
	nextPaceTime = currentTime() + paceTime();
}

void Resident::pace() {
	if (currentTime() >= nextPaceTime) {
		double newLoc[2];
		paceLocation(newLoc);
		commandToLoc(newLoc[0], newLoc[1], 1.0, PHYSICS);
		resetPaceClock();
	}
}

double Resident::personalSpace() {
	return INSANE_PERSONAL_SPACE + (SANE_PERSONAL_SPACE - INSANE_PERSONAL_SPACE) * sanity;
}

void Resident::resolvePersonalSpace(NocturnePerson *otherPerson) {
	if (otherPerson == this) return;
	double xDif = x_loc - otherPerson->getX();
	double yDif = y_loc - otherPerson->getY();
	double distance = sqrt(xDif * xDif + yDif * yDif);

	if (distance < personalSpace()) putDistanceBetween(otherPerson, personalSpace());
}

void Resident::putDistanceBetween(NocturneObject *otherThing, double distance) {
	double dir[2];
	dir[0] = x_loc - otherThing->getX();
	dir[1] = y_loc - otherThing->getY();
	double mag = sqrt(dir[0] * dir[0] + dir[1] * dir[1]);
	dir[0] /= mag;
	dir[1] /= mag;
	dir[0] *= distance;
	dir[1] *= distance;

	if (mag > distance + PERSONAL_SPACE_MARGIN_OF_ERROR ||
		mag < distance - PERSONAL_SPACE_MARGIN_OF_ERROR) commandToLoc(otherThing->getX() + dir[0], otherThing->getY() + dir[1], 1.0, PHYSICS);
}

void Resident::moveTowardsKeeper() {
	if (Keeper::theKeeper) putDistanceBetween(Keeper::theKeeper, FOLLOW_DIST);
}

void Resident::computeBehavior(double timeLapsed) {
	// Follow the keeper if necessary.
	if (isFollowingKeeper) moveTowardsKeeper();
	else pace();

	// Super behavior.
	NocturnePerson::computeBehavior(timeLapsed);

	// Reduce radius to normal.
	radius -= SHRINK_SPEED * timeLapsed;
	if (radius < PERSON_RADIUS) radius = PERSON_RADIUS;

	// Singing crazy radius
	if (isSinging) radius = PERSON_RADIUS + (MAX_SING_SIZE - PERSON_RADIUS) * RandomGenerator::randomFloat();

	// Resident collisions.
	for (int i = 0; i < Resident::allResidents.size(); i++) {
		Resident *otherRes = Resident::allResidents[i];
		checkAndHandleCollision(otherRes);
		if (!isFollowingKeeper) resolvePersonalSpace(otherRes);
	}

	// Keeper collisions.
	if (Keeper::theKeeper) {
		checkAndHandleCollision(Keeper::theKeeper);
		if (!isFollowingKeeper) resolvePersonalSpace(Keeper::theKeeper);
	}

	// Chatter
	if (!isNPC()) chatter();
}

void Resident::collisionBehavior(NocturnePerson* otherPerson, double collisionStrength) {
	// Turn red.
	//color[1] = max(0.0, color[1] - collisionStrength);
	//color[2] = max(0.0, color[2] - collisionStrength);
}

void Resident::followKeeper() { isFollowingKeeper = true; canChangeRooms = true; }
void Resident::stopFollowingKeeper() { isFollowingKeeper = false; canChangeRooms = false; }


bool Resident::currentlyFollowingKeeper() {
	return isFollowingKeeper;
}

bool Resident::currentlyCanChangeRooms() {
	return canChangeRooms;
}

void Resident::highlight() {
	isHighlighted = true;
}

void Resident::unhighlight() {
	isHighlighted = false;
}

bool Resident::currentlyHighlighted() {
	return isHighlighted;
}


void Resident::onRoomChange(Room *newRoom) {
	newRoom->addResident(this);

	if (oldRoom != NULL) {
		oldRoom->removeResident(this);
	}

	if (Keeper::theKeeper && Keeper::theKeeper->getRoom() == newRoom) {
		startChirping();
		if (oldRoom) {
			vector<Resident *> residents_in_room = newRoom->getResidents();
			for (int i = 0; i < residents_in_room.size(); i++) {
				residents_in_room[i]->sing();
			}
		}
	} else {
		stopChirping();
	}

}


void Resident::draw() {
	NocturnePerson::draw();

	if (isFollowingKeeper) {
		glColor3f(1.0, 0.0, 0.0);
		glBegin( GL_TRIANGLE_FAN );
	    for (int j = 0; j < NUM_PERSON_POINTS; j++) {
	        GLfloat x_pos = x_loc + FOLLOW_INDICATOR_RADIUS * ::cos(TWOPI * j / (double) NUM_PERSON_POINTS);
	        GLfloat y_pos = y_loc + FOLLOW_INDICATOR_RADIUS * ::sin(TWOPI * j / (double) NUM_PERSON_POINTS);
	        glVertex2f(x_pos, y_pos);
	    }
	    glEnd();
	}
}

string Resident::getNextQuote() {
	/*static int mNum = 0;
	string message;
	switch(mNum) {
		case 0:
		message = "Hi.";
		break;

		case 1:
		message = "You look a little blue.";
		break;

		case 2:
		message = "Space?";
		break;

		case 3:
		message = "OMG I'M A DOT!";
		break;

		case 4:
		message = "My body lies over the ocean. My body lies over the sea.";
		break;
	}
	mNum++;
	mNum %= 5;*/

	vector<string> chatter = getArea()->getChatter();
	int messageNum = int(floor(RandomGenerator::randomFloat() * chatter.size()));
	if (messageNum == chatter.size()) messageNum--;
	if (messageNum == -1) return "Hi.";
	return chatter[messageNum];
}

void Resident::scheduleChatter() {
	nextChatterTime = currentTime() + (CHATTER_MEAN_PAUSE_TIME - CHATTER_FLUX_PAUSE_TIME + (2.0 * CHATTER_FLUX_PAUSE_TIME * RandomGenerator::randomFloat()));
}

void Resident::chatter() {
	double theTime = currentTime();
	if (nextChatterTime <= theTime) {
		currentChatter = getNextQuote();
		scheduleChatter();
		prevChatterTime = theTime;
	}
	double timeIntoChatter = theTime - prevChatterTime;
	NocturneMessage::chatter(x_loc, y_loc, currentChatter, timeIntoChatter);
}
