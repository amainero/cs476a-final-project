#ifdef __MACOSX_CORE__
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
  #include <GL/glut.h>
#endif

#include "NocturneObject.h"
#include "NocturnePerson.h"
#include "Keeper.h"
#include "Room.h"
#include <vector>

#define AREA_SIZE (ROOM_SIZE * 3.0)
#define WORLD_OFFSET (AREA_SIZE / 2.0)

#define CAMERA_MOVE_SPEED 0.75

#ifndef NOCTURNE_CAMERA_H
#define NOCTURNE_CAMERA_H

#define FOLLOW_KEEPER false

#define DARK_START 50
#define DARK_END (DARK_START + 200)
#define DARK_RES_DECREASE 2

#define MIN_DEPRESSION_MULT 0.5
#define HAPPINESS_FADE_SPEED 0.5

#define DARK_FADE_UP_TIME 5.0
#define DARK_FADE_OUT_TIME 1.0

class NocturneCamera: public NocturneObject {
public:
	int windowWidth, windowHeight;

	NocturneCamera();

	void setCamera();

	void applyDarkness();
	void fadeOut();
	void fadeOutWithSpeed(double speed);
	void fadeUpAfterDelay(double delay);
	void fadeUpAfterDelayWithSpeed(double delay, double speed);

	void makeExempt(NocturnePerson* person);

private:
	int darkStart, darkEnd;
	double distance;
	double destX;
	double destY;

	double fadeStart;
	double fadeDelay;
	bool isFadingUp;
	double fadeUpTime;
	bool isFadingOut;
	double fadeOutTime;
	int initFadeOutDarkStart, initFadeOutDarkEnd;
	double happinessLevel;

	int prevDarkX, prevDarkY;

	std::vector<NocturnePerson*> *exempt;

	virtual void computeBehavior(double timeLapsed);
};

#endif