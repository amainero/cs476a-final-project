#ifdef __MACOSX_CORE__
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
  #include <GL/glut.h>
#endif

#include "NocturneConstants.h"
#include "Timing.h"
#include <math.h>

#ifndef NOCTURNE_OBJECT_H
#define NOCTURNE_OBJECT_H

class NocturneObject;
class Board;
class Room;
class Area;
class AssetManager;

// NOTE: Pysics mode does not take command time into account.
enum NocturneObjectMoveMethod { LINEAR, SINE, PHYSICS };

struct NocturneObjectMoveCommand {
	NocturneObjectMoveCommand(NocturneObject* p, double x, double y, double commandTime, NocturneObjectMoveMethod method);
	bool updateAndCheckFinish(double timeLapsed);

	double goal_x, goal_y;
	double start_x, start_y;
	double timeLeft;
	double timeForCommand;
	NocturneObjectMoveMethod method;

	NocturneObject *object;
};

class NocturneObject {
public:
	NocturneObject();

	virtual void commandToLoc(double x, double y, double commandTime, NocturneObjectMoveMethod method);
	void commandToRelativeLoc(double x, double y, double commandTime, NocturneObjectMoveMethod method);
	void computeFrame();

	void getScreenCoords(int* data);
	static void getScreenCoordsForPoint(double x, double y, int* data);
	Room* getRoom();
	Area* getArea();
	double getX();
	double getY();

	static Board* theBoard;
	static AssetManager *assetManager;

	friend struct NocturneObjectMoveCommand;
protected:
	double x_loc, y_loc;
	double lastTime;
	NocturneObjectMoveCommand *currentMoveCommand;

	virtual void computeBehavior(double timeLapsed);
};

#endif