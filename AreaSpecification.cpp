#include "AreaSpecification.h"
#include "AssetManager.h"
#include "NocturneNPC.h"

#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

enum AreaSpecReadingMode {
	ID,
	LOCKED_ROOM,
	ROOMS
};


AreaSpecification::AreaSpecification(string filename) {
	locked_room_x = locked_room_y = -1;
	num_rooms = 0;
	readFile(filename);
}


AreaSpecification::AreaSpecification(AreaSpecification *unrotated_specification) {
	locked_room_x = locked_room_y = -1;
	this->num_rooms = unrotated_specification->num_rooms;
	this->id = unrotated_specification->id + "_R";

	for (int i = 0; i < GRID_DIM_SIZE; i++) {
		for (int j = 0; j < GRID_DIM_SIZE; j++) {
			// TODO: make sure this works
			int rotated_x = GRID_DIM_SIZE - j - 1;
			int rotated_y = i;
			if (unrotated_specification->roomExists(i, j)) {
				room_exists[rotated_x][rotated_y] = true;
				initial_room_chord_definitions[rotated_x][rotated_y] = unrotated_specification->getInitialRoomChordDefinition(i, j);
				if (unrotated_specification->roomIsLocked(i, j)) {
					locked_room_x = rotated_x;
					locked_room_y = rotated_y;
				}
			} else {
				room_exists[rotated_x][rotated_y] = false;
			}
		}
	}
}


bool AreaSpecification::roomExists(int relative_x, int relative_y) {
	return room_exists[relative_x][relative_y];
}


bool AreaSpecification::roomIsLocked(int relative_x, int relative_y) {
	return (relative_x == locked_room_x) && (relative_y == locked_room_y);
}


Chord AreaSpecification::getInitialRoomChordDefinition(int relative_x, int relative_y) {
	return initial_room_chord_definitions[relative_x][relative_y];
}


void AreaSpecification::readFile(string filename) {
	AreaSpecReadingMode reading_mode = ID;
	ifstream input_stream(filename);
	string line;

	int current_room_row;

	while (getline(input_stream, line)) {

		if (line.length() == 0) {
			continue;
		}

		if (line[0] == '#') {
			continue;
		}

		if (line == "ID") {
			reading_mode = ID;
			continue;
		} else if (line == "ROOMS") {
			reading_mode = ROOMS;
			current_room_row = GRID_DIM_SIZE - 1;
			continue;
		} else if (line == "LOCKED_ROOM") {
			reading_mode = LOCKED_ROOM;
			continue;
		}

		switch (reading_mode) {
			case ID: 
				readID(line);
				break;
			case ROOMS:
				readRoomRow(line, current_room_row);
				current_room_row--;
				break;
			case LOCKED_ROOM:
				readLockedRoom(line);
				break;
			default:
				break;
		}
	}

}


void AreaSpecification::readID(string line) {
	this->id = line;
}


void AreaSpecification::readRoomRow(string line, int current_room_row) {
	vector<string> room_row_elems;
	AssetManager::split(line, room_row_elems);

	if (room_row_elems.size() != GRID_DIM_SIZE) {
		cout << "Error reading room row: " << line << endl;
		return;
	}

	for (int i = 0; i < GRID_DIM_SIZE; i++) {
		if (room_row_elems[i] == "N") {
			room_exists[i][current_room_row] = false;
		} else if (room_row_elems[i] == "E") {
			room_exists[i][current_room_row] = true;
			Chord empty_chord;
			initial_room_chord_definitions[current_room_row][i] = empty_chord;
			num_rooms++;
		} else {
			room_exists[i][current_room_row] = true;
			string chord_id = room_row_elems[i];
			Chord chord = NocturneObject::assetManager->getChord(chord_id);
			initial_room_chord_definitions[i][current_room_row] = chord;
			num_rooms++;
		}
	}
}


void AreaSpecification::readLockedRoom(string line) {
	vector<string> locked_row_elems;
	AssetManager::split(line, locked_row_elems);

	if (locked_row_elems.size() != 2) {
		cout << "Error reading locked row: " << line << endl;
		return;
	}

	// set coordinates
	locked_room_x = stoi(locked_row_elems[0]);
	locked_room_y = stoi(locked_row_elems[1]);
}

