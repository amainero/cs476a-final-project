#include "AssetManager.h"
#include "AreaSpecification.h"

#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>

using namespace std;

enum AssetReadingMode {
	DIALOG_FILE,
	ITEM_FILE,
	AREA_FILE,
	QUEST_FILE,
	CHORD_FILE,
	NO_ASSET
};

#define ASSET_MASTER_LIST "assets/master.txt"

AssetManager::AssetManager() {

	NocturneObject::assetManager = this;

	string filename = ASSET_MASTER_LIST;
	ifstream input_stream(filename);
	string line;

	AssetReadingMode reading_mode = NO_ASSET;

	while (getline(input_stream, line)) {
		if (line.length() == 0) {
			continue;
		}

		if (line == "DIALOG") {
			reading_mode = DIALOG_FILE;
			continue;
		} else if (line == "ITEM") {
			reading_mode = ITEM_FILE;
			continue;
		} else if (line == "AREA") {
			reading_mode = AREA_FILE;
			continue;
		} else if (line == "QUEST") {
			reading_mode = QUEST_FILE;
			continue;
		} else if (line == "CHORDS") {
			reading_mode = CHORD_FILE;
			continue;
		}

		string asset_filename = line;

		if (reading_mode == DIALOG_FILE) {
			NocturneDialog *dialog = new NocturneDialog(asset_filename);
			dialog_assets[dialog->getID()] = dialog;
			continue;
		} else if (reading_mode == ITEM_FILE) {
			Item *item = new Item(asset_filename);
			item_assets[item->id] = item;
			continue;
		} else if (reading_mode == AREA_FILE) {
			AreaSpecification *unrotated_spec = new AreaSpecification(asset_filename);
			area_spec_assets[unrotated_spec->id] = unrotated_spec;

			AreaSpecification *first_rotated_spec = new AreaSpecification(unrotated_spec);
			AreaSpecification *second_rotated_spec = new AreaSpecification(first_rotated_spec);
			AreaSpecification *third_rotated_spec = new AreaSpecification(second_rotated_spec);
			
			area_spec_assets[first_rotated_spec->id] = first_rotated_spec;
			area_spec_assets[second_rotated_spec->id] = second_rotated_spec;
			area_spec_assets[third_rotated_spec->id] = third_rotated_spec;
			continue;
		} else if (reading_mode == QUEST_FILE) {
			QuestSpecification *quest_specification = new QuestSpecification(asset_filename);
			quest_spec_assets[quest_specification->id] = quest_specification;
			continue;
		} else if (reading_mode == CHORD_FILE) {
			readChordFile(line);
			continue;
		}
	}
}


NocturneDialog *AssetManager::getDialog(string dialog_id) {
	if (dialog_assets.find(dialog_id) != dialog_assets.end()) {
		return dialog_assets[dialog_id];
	}
	return NULL;
}


Item *AssetManager::getItem(string item_id) {
	if (item_assets.find(item_id) != item_assets.end()) {
		return item_assets[item_id];
	}
	return NULL;
}


AreaSpecification *AssetManager::getAreaSpecification(string area_spec_id) {
	if (area_spec_assets.find(area_spec_id) != area_spec_assets.end()) {
		return area_spec_assets[area_spec_id];
	}
	return NULL;
}


QuestSpecification *AssetManager::getQuestSpecification(string quest_spec_id) {
	if (quest_spec_assets.find(quest_spec_id) != quest_spec_assets.end()) {
		return quest_spec_assets[quest_spec_id];
	}
	return NULL;
}


Chord AssetManager::getChord(string chord_id) {
	if (chord_assets.find(chord_id) != chord_assets.end()) {
		return chord_assets[chord_id];
	}

	Chord empty_chord;
	return empty_chord;
}


AreaSpecification *AssetManager::getRandomAreaSpecification() {
	int random_index = RandomGenerator::randomInt(0, area_spec_assets.size() - 1);
	map<string, AreaSpecification *>::iterator it = area_spec_assets.begin();
	std::advance(it, random_index);
	return it->second;
}


AreaSpecification *AssetManager::getCompatibleAreaSpecification(
	Direction dir,
	int prev_room_relative_x,
	int prev_room_relative_y
) {
	vector<AreaSpecification *> compatible_area_specifications;
	map<string, AreaSpecification *>::iterator it;

	for (it = area_spec_assets.begin(); it != area_spec_assets.end(); it++) {
		AreaSpecification *next_spec = it->second;
		if (areaCompatible(next_spec, dir, prev_room_relative_x, prev_room_relative_y)) {
			compatible_area_specifications.push_back(next_spec);
		}
	}

	if (compatible_area_specifications.size() == 0) {
		return NULL;
	}

	int random_index = RandomGenerator::randomInt(0, compatible_area_specifications.size() - 1);
	return compatible_area_specifications[random_index];
}


QuestSpecification* AssetManager::getCompatibleQuestSpecification(AreaSpecification *area_spec) {
	vector<QuestSpecification *> compatible_quest_specifications;
	map<string, QuestSpecification *>::iterator it;

	for (it = quest_spec_assets.begin(); it != quest_spec_assets.end(); it++) {
		QuestSpecification *next_spec = it->second;
		if (questCompatible(area_spec, next_spec)) {
			compatible_quest_specifications.push_back(next_spec);
		}
	}

	if (compatible_quest_specifications.size() == 0) {
		return NULL;
	}

	// get a random compatible spec
	int random_index = RandomGenerator::randomInt(0, compatible_quest_specifications.size() - 1);

	// add to list of used specs
	used_quest_spec_ids.push_back(compatible_quest_specifications[random_index]->id);
	return compatible_quest_specifications[random_index];
}


bool AssetManager::questCompatible(AreaSpecification *area_spec, QuestSpecification *quest_spec) {
	// TODO: if there are compatibility issues later, here's where we determine what they are
	for (int i = 0; i < used_quest_spec_ids.size(); i++) {
		if (quest_spec->id == used_quest_spec_ids[i]) {
			return false;
		}
	}
	int num_quest_groups = quest_spec->npc_groups.size();
	int num_available_rooms = area_spec->num_rooms - 1;

	if (num_available_rooms <= num_quest_groups) {
		return false;
	}
	return true;
}


bool AssetManager::areaCompatible(
	AreaSpecification *area_spec,
	Direction dir,
	int prev_room_relative_x,
	int prev_room_relative_y
) {
	int next_room_relative_x = prev_room_relative_x;
	int next_room_relative_y = prev_room_relative_y;

	switch (dir) {
		case NORTH:
			next_room_relative_y = 0;
			break;
		case SOUTH:
			next_room_relative_y = GRID_DIM_SIZE - 1;
			break;
		case EAST:
			next_room_relative_x = 0;
			break;
		case WEST:
			next_room_relative_x = GRID_DIM_SIZE - 1;
			break;
	}
	return area_spec->roomExists(next_room_relative_x, next_room_relative_y) && 
		 (!area_spec->roomIsLocked(next_room_relative_x, next_room_relative_y));
}



void AssetManager::readChordFile(string filename) {
	ifstream input_stream(filename);
	string line;

	while (getline(input_stream, line)) {
		if (line.length() == 0) {
			continue;
		}

		vector<string> chord_elems;
		AssetManager::split(line, chord_elems);

		if (chord_elems.size() < 2) {
			cout << "Error reading chord: " << line << endl;
		}

		string chord_id = chord_elems[0];
		Chord chord;

		for (int i = 1; i < chord_elems.size(); i++) {
			Note note;
			note.semitone = stoi(chord_elems[i]);
			chord.notes.push_back(note);
		}

		chord_assets[chord_id] = chord;
	}

}


// taken from http://stackoverflow.com/questions/236129/split-a-string-in-c
vector<string> &AssetManager::split(const string &s, vector<string> &elems, char delimiter) {
    std::stringstream ss(s);
    std::string item;

   	while (getline(ss, item, delimiter)) {
        elems.push_back(item);
    }
    return elems;
}
