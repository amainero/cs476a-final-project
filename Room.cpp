#ifdef __MACOSX_CORE__
  // note: for mac only
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
  #include <GL/glut.h>
#endif

#include "Room.h"
#include <iostream>
#include <algorithm>

#define LOCKED_ROOM_DOOR_DEPTH (DOOR_SIZE / 5)


Room::Room(
		int area_absolute_x,
		int area_absolute_y,
		int area_relative_x, 
		int area_relative_y
) {
	this->area_absolute_x = area_absolute_x;
	this->area_absolute_y = area_absolute_y;
	this->area_relative_x = area_relative_x;
	this->area_relative_y = area_relative_y;

    this->is_locked = false;
}


void Room::setWall(Direction dir, bool is_open) {
	switch (dir) {
		case NORTH:
			north_wall_open = is_open;
			break;
		case SOUTH:
			south_wall_open = is_open;
			break;
		case EAST:
			east_wall_open = is_open;
			break;
		case WEST:
			west_wall_open = is_open;
			break;
	}
}

bool Room::wallIsOpen(Direction dir) {
    if (is_locked) {
        return false;
    }

    switch(dir) {
        case NORTH:
            return north_wall_open;
        case SOUTH:
            return south_wall_open;
        case EAST:
            return east_wall_open;
        case WEST:
            return west_wall_open;
    }
    return false;
}


void Room::createInitialResidentsFromNotes(vector<Note> notes) {
	for (int i = 0; i < notes.size(); i++) {
		Resident *resident = new Resident();
        setResidentRandomLocation(resident);
		resident->note = notes[i];

        // current residents are added in first onRoomChange call
        target_residents.push_back(resident);
	}

    // sort target residents for later comparison
    std::sort(target_residents.begin(), target_residents.end(), Resident::compare);
}


std::vector<Resident *> Room::getResidents() {
	return current_residents;
}


std::vector<Resident *> Room::getTargetResidents() {
    return target_residents;
}


bool Room::containsTargetResidents() {
    if (current_residents.size() != target_residents.size()) {
        return false;
    }


    // since the list is guaranteed to be sorted, we compare notes in order
    for (int i = 0; i < current_residents.size(); i++) {
        if (Resident::compare(current_residents[i], target_residents[i]) != 0) {
            // case: different semitones
            return false;
        }
    }
    return true;
}


void Room::addResident(Resident *resident) {
    current_residents.push_back(resident);
    // after adding a new resident, we sort by semitone
    // this is to make containsTargetResidents more efficient
    std::sort(current_residents.begin(), current_residents.end(), Resident::compare);
}


void Room::removeResident(Resident *resident) {
    vector<Resident *>::iterator it = find(current_residents.begin(), current_residents.end(), resident);
    if (it != current_residents.end()) {
        current_residents.erase(it);
    }
}


void Room::setResidentRandomLocation(Resident *resident) {
    GLfloat room_relative_x = RandomGenerator::randomFloat();
    GLfloat room_relative_y = RandomGenerator::randomFloat();

    GLfloat resident_x_pos = (room_relative_x + area_absolute_x + area_relative_x) * ROOM_SIZE - ROOM_DRAW_OFFSET_X;
    GLfloat resident_y_pos = (-room_relative_y + area_absolute_y + area_relative_y) * ROOM_SIZE - ROOM_DRAW_OFFSET_Y;
    resident->setLocation(resident_x_pos, resident_y_pos);
}


int Room::getAbsoluteX() {
    return area_absolute_x + area_relative_x;
}


int Room::getAbsoluteY() {
    return area_absolute_y + area_relative_y;
}


int Room::getRelativeX() {
    return area_relative_x;
}


int Room::getRelativeY() {
    return area_relative_y;
}


void Room::setLocked(bool is_locked) {
    this->is_locked = is_locked;
}

bool Room::isLocked() {
    return is_locked;
}

void Room::setUnlockConditions(NocturneNPC *npc, Item *item) {
    unlock_condition_npc = npc;
    unlock_condition_item = item;
}


bool Room::canBeUnlocked(NocturneNPC *npc, Item *item) {

    // check to see if we have the right follower
    if (unlock_condition_npc) {
        if (!npc) {
            return false;
        }
        if (unlock_condition_npc->getID() != npc->getID()) {
            return false;
        }
    }

    // check to see if we have the right item
    if (unlock_condition_item) {
        if (!item) {
            return false;
        }
        if (unlock_condition_item->id != item->id) {
            return false;
        }
    }

    return true;
}


void Room::drawLockIndicators() {
    GLfloat top_left_x = (area_absolute_x + area_relative_x) * ROOM_SIZE - ROOM_DRAW_OFFSET_X;
    GLfloat top_left_y = (area_absolute_y + area_relative_y) * ROOM_SIZE - ROOM_DRAW_OFFSET_Y;

    // TODO: lack of GRID_DIM_SIZE breaks encapsulation
    if (area_relative_y != 2) {
        // NORTH
        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x + DOOR_START_DIST, top_left_y + LOCKED_ROOM_DOOR_DEPTH);
        glVertex2f(top_left_x + DOOR_START_DIST, top_left_y - LOCKED_ROOM_DOOR_DEPTH);
        glEnd();

        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x + DOOR_END_DIST, top_left_y + LOCKED_ROOM_DOOR_DEPTH);
        glVertex2f(top_left_x + DOOR_END_DIST, top_left_y - LOCKED_ROOM_DOOR_DEPTH);
        glEnd();
    }

    if (area_relative_y != 0) {
        // SOUTH
        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x + DOOR_START_DIST, top_left_y - ROOM_SIZE - LOCKED_ROOM_DOOR_DEPTH);
        glVertex2f(top_left_x + DOOR_START_DIST, top_left_y - ROOM_SIZE + LOCKED_ROOM_DOOR_DEPTH);
        glEnd();

        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x + DOOR_END_DIST, top_left_y - ROOM_SIZE - LOCKED_ROOM_DOOR_DEPTH);
        glVertex2f(top_left_x + DOOR_END_DIST, top_left_y - ROOM_SIZE + LOCKED_ROOM_DOOR_DEPTH);
        glEnd();
    }

    if (area_relative_x != 2) {
        // EAST
        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x + ROOM_SIZE - LOCKED_ROOM_DOOR_DEPTH, top_left_y - DOOR_START_DIST);
        glVertex2f(top_left_x + ROOM_SIZE + LOCKED_ROOM_DOOR_DEPTH, top_left_y - DOOR_START_DIST);
        glEnd();

        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x + ROOM_SIZE - LOCKED_ROOM_DOOR_DEPTH, top_left_y - DOOR_END_DIST);
        glVertex2f(top_left_x + ROOM_SIZE + LOCKED_ROOM_DOOR_DEPTH, top_left_y - DOOR_END_DIST);
        glEnd();
    }

    if (area_relative_x != 0) {
        // WEST
        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x - LOCKED_ROOM_DOOR_DEPTH, top_left_y - DOOR_START_DIST);
        glVertex2f(top_left_x + LOCKED_ROOM_DOOR_DEPTH, top_left_y - DOOR_START_DIST);
        glEnd();

        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x - LOCKED_ROOM_DOOR_DEPTH, top_left_y - DOOR_END_DIST);
        glVertex2f(top_left_x + LOCKED_ROOM_DOOR_DEPTH, top_left_y - DOOR_END_DIST);
        glEnd();
    }

    if (!is_locked) {
        glColor3f(0.4, 0.4, 0.4);
        // draw center indication square
        glBegin( GL_QUADS );
            glVertex2f(top_left_x + RUG_START_DIST, top_left_y - RUG_START_DIST);
            glVertex2f(top_left_x + RUG_END_DIST, top_left_y - RUG_START_DIST);
            glVertex2f(top_left_x + RUG_END_DIST, top_left_y - RUG_END_DIST);
            glVertex2f(top_left_x + RUG_START_DIST, top_left_y - RUG_END_DIST);
        glEnd();
    }
}


void Room::draw(float color[3]) {
    GLfloat top_left_x = (area_absolute_x + area_relative_x) * ROOM_SIZE - ROOM_DRAW_OFFSET_X;
    GLfloat top_left_y = (area_absolute_y + area_relative_y) * ROOM_SIZE - ROOM_DRAW_OFFSET_Y;

    if (is_locked) {
        color[0] = 1.0;
        color[1] = 1.0;
        color[2] = 1.0;
    }
    
    glColor3f(color[0], color[1], color[2]);
    glBegin(GL_QUADS);
        glVertex2f(top_left_x, top_left_y);
        glVertex2f(top_left_x + ROOM_SIZE, top_left_y);
        glVertex2f(top_left_x + ROOM_SIZE, top_left_y - ROOM_SIZE);
        glVertex2f(top_left_x, top_left_y - ROOM_SIZE);
    glEnd();


    glColor3f(1.0, 1.0, 1.0);

    // draw NORTH wall
    glBegin( GL_LINE_STRIP );
    glVertex2f(top_left_x, top_left_y);
    if (wallIsOpen(NORTH)) {
        glVertex2f(top_left_x + DOOR_START_DIST, top_left_y);
        glEnd();

        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x + DOOR_END_DIST, top_left_y);
    }
    glVertex2f(top_left_x + ROOM_SIZE, top_left_y);
    glEnd();


    // draw EAST wall
    glBegin( GL_LINE_STRIP );
    glVertex2f(top_left_x + ROOM_SIZE, top_left_y);
    if (wallIsOpen(EAST)) {
        glVertex2f(top_left_x + ROOM_SIZE, top_left_y - DOOR_START_DIST);
        glEnd();

        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x + ROOM_SIZE, top_left_y - DOOR_END_DIST);
    }
    glVertex2f(top_left_x + ROOM_SIZE, top_left_y - ROOM_SIZE);
    glEnd();

    // draw WEST wall
    glBegin( GL_LINE_STRIP );
    glVertex2f(top_left_x, top_left_y);
    if (wallIsOpen(WEST)) {
        glVertex2f(top_left_x, top_left_y - DOOR_START_DIST);
        glEnd();

        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x, top_left_y - DOOR_END_DIST);
    }
    glVertex2f(top_left_x, top_left_y - ROOM_SIZE);
    glEnd();

    // draw SOUTH wall
    glBegin( GL_LINE_STRIP );
    glVertex2f(top_left_x, top_left_y - ROOM_SIZE);
    if (wallIsOpen(SOUTH)) {
        glVertex2f(top_left_x + DOOR_START_DIST, top_left_y - ROOM_SIZE);
        glEnd();

        glBegin( GL_LINE_STRIP );
        glVertex2f(top_left_x + DOOR_END_DIST, top_left_y - ROOM_SIZE);
    }
    glVertex2f(top_left_x + ROOM_SIZE, top_left_y - ROOM_SIZE);
    glEnd();
}