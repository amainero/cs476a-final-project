#include "NoteLogic.h"
#include <math.h>
#include <iostream>

using namespace std;

// chord types
#define CHORD_MAJOR_TRIAD 0
#define CHORD_DOM_7 1
#define CHORD_MINOR_TRIAD 2
#define CHORD_FULL_DIM_7 3
#define CHORD_HALF_DIM_7 4
#define CHORD_DIM_TRIAD 5
#define CHORD_POWER 6
#define CHORD_SINGLE_NOTE 7
#define CHORD_MINOR_THIRD 8
#define NUM_CHORD_TYPES 9

// chord frequencies
#define CHORD_FREQ_MAJOR_TRIAD 0.0
#define CHORD_FREQ_DOM_7 0.1
#define CHORD_FREQ_MINOR_TRIAD 0.25
#define CHORD_FREQ_FULL_DIM_7 0.08
#define CHORD_FREQ_HALF_DIM_7 0.05
#define CHORD_FREQ_DIM_TRIAD 0.04
#define CHORD_FREQ_POWER 0.18
#define CHORD_FREQ_SINGLE_NOTE 0.1
#define CHORD_FREQ_MINOR_THIRD 0.2

// mode types
#define MODE_IONIAN 0
#define MODE_DORIAN 1
#define MODE_PHRYGIAN 2
#define MODE_LYDIAN 3
#define MODE_MIXOLYDIAN 4
#define MODE_AEOLIAN 5
#define MODE_LOCRIAN 6

// mode frequencies
#define MODE_FREQ_IONIAN 0.24
#define MODE_FREQ_DORIAN 0.12
#define MODE_FREQ_PHRYGIAN 0.12
#define MODE_FREQ_LYDIAN 0.15
#define MODE_FREQ_MIXOLYDIAN 0.18
#define MODE_FREQ_AEOLIAN 0.1
#define MODE_FREQ_LOCRIAN 0.09

// mode steps (minor key)
#define MODE_STEP_IONIAN 0
#define MODE_STEP_DORIAN 2
#define MODE_STEP_PHRYGIAN 3
#define MODE_STEP_LYDIAN 5
#define MODE_STEP_MIXOLYDIAN 7
#define MODE_STEP_AEOLIAN 8
#define MODE_STEP_LOCRIAN 10

// octave range
#define MIN_OCTAVE 3
#define MAX_OCTAVE 7

// octave frequencies
#define OCTAVE_3_FREQ 0.3
#define OCTAVE_4_FREQ 0.3
#define OCTAVE_5_FREQ 0.18
#define OCTAVE_6_FREQ 0.12
#define OCTAVE_7_FREQ 0.1

// computing frequency from pitch
#define SEMITONE_REL_FACTOR 0.08333 // 2^(1/12)
#define A_OCTAVE 3
#define A3_FREQUENCY 220 

// chord const expander sentinel (means that slot in chord is empty)
#define CCXS -1
#define MAX_BASE_CHORD_SIZE 4


const int chord_definitions[NUM_CHORD_TYPES][MAX_BASE_CHORD_SIZE] = {
	{0, 4, 7, CCXS},		// major triad
	{0, 4, 7, 10},			// dom 7
	{0, 3, 7, CCXS},		// minor triad
	{0, 3, 6, 9},			// fully diminished 7
	{0, 3, 6, 10},			// half diminished 7
	{0, 3, 6, CCXS},		// diminished triad
	{0, 7, CCXS, CCXS},		// power chord
	{0, CCXS, CCXS, CCXS},	// single note
	{0, 3, CCXS, CCXS}		// minor third
};


double NoteLogic::getFrequencyForNote(Note *note) {
	// get num half steps up from A
	// assume A3 is semitone 0
	int half_steps_from_a3 = note->semitone;
	return ::pow(SEMITONE_REL_FACTOR, half_steps_from_a3) * A3_FREQUENCY;
}


// vector<Note *> NoteLogic::generateNotesForNewRoom() {
// 	int chord_type = generateRandomChordType();
// 	int mode_type = generateRandomModeType();
// 	int octave = generateRandomOctave();
// 	int mode_step = getModeStep(mode_type);
// 	vector<Note *> notes_for_new_room;

// 	// get the chord defintion
// 	for (int i = 0; i < MAX_BASE_CHORD_SIZE; i++) {
// 		if (chord_definitions[chord_type][i] == CCXS) {
// 			break;
// 		}
// 		Note* note_in_chord = new Note;
// 		// get semitone + octave
// 		int note_octave = octave;
// 		int note_semitone = chord_definitions[chord_type][i] + mode_step;
// 		while (note_semitone > NUM_SEMITONES_IN_OCTAVE) {
// 			note_octave++;
// 			note_semitone -= NUM_SEMITONES_IN_OCTAVE;
// 		}
// 		note_in_chord->semitone = note_semitone;
// 		note_in_chord->octave = note_octave;
// 		notes_for_new_room.push_back(note_in_chord);
// 	}
// 	return notes_for_new_room;
// }


// int NoteLogic::generateRandomChordType() {
// 	double rand_chord_gen = RandomGenerator::randomFloat();
// 	double cur_chord_threshold = 0.0;
// 	int cur_chord_type = -1;

// 	while (rand_chord_gen > cur_chord_threshold) {
// 		cur_chord_type++;
// 		switch (cur_chord_type) {
// 			case CHORD_MAJOR_TRIAD:
// 				cur_chord_threshold += CHORD_FREQ_MAJOR_TRIAD;
// 				break;
// 			case CHORD_DOM_7:
// 				cur_chord_threshold += CHORD_FREQ_DOM_7;
// 				break;
// 			case CHORD_MINOR_TRIAD:
// 				cur_chord_threshold += CHORD_FREQ_MINOR_TRIAD;
// 				break;
// 			case CHORD_FULL_DIM_7:
// 				cur_chord_threshold += CHORD_FREQ_FULL_DIM_7;
// 				break;
// 			case CHORD_HALF_DIM_7:
// 				cur_chord_threshold += CHORD_FREQ_HALF_DIM_7;
// 				break;
// 			case CHORD_DIM_TRIAD:
// 				cur_chord_threshold += CHORD_FREQ_DIM_TRIAD;
// 				break;
// 			case CHORD_POWER:
// 				cur_chord_threshold += CHORD_FREQ_POWER;
// 				break;
// 			case CHORD_SINGLE_NOTE:
// 				cur_chord_threshold += CHORD_FREQ_SINGLE_NOTE;
// 				break;
// 			case CHORD_MINOR_THIRD:
// 				cur_chord_threshold += CHORD_FREQ_MINOR_THIRD;
// 				break;
// 		}
// 	}
// 	return cur_chord_type;
// }


// int NoteLogic::generateRandomModeType() {
// 	double rand_mode_gen = RandomGenerator::randomFloat();
// 	double cur_mode_threshold = 0.0;
// 	int cur_mode_type = -1;

// 	while (rand_mode_gen > cur_mode_threshold) {
// 		cur_mode_type++;
// 		switch (cur_mode_type) {
// 			case MODE_IONIAN:
// 				cur_mode_threshold += MODE_FREQ_IONIAN;
// 				break;
// 			case MODE_DORIAN:
// 				cur_mode_threshold += MODE_FREQ_DORIAN;
// 				break;
// 			case MODE_PHRYGIAN:
// 				cur_mode_threshold += MODE_FREQ_PHRYGIAN;
// 				break;
// 			case MODE_LYDIAN:
// 				cur_mode_threshold += MODE_FREQ_LYDIAN;
// 				break;
// 			case MODE_MIXOLYDIAN:
// 				cur_mode_threshold += MODE_FREQ_MIXOLYDIAN;
// 				break;
// 			case MODE_AEOLIAN:
// 				cur_mode_threshold += MODE_FREQ_AEOLIAN;
// 				break;
// 			case MODE_LOCRIAN:
// 				cur_mode_threshold += MODE_FREQ_LOCRIAN;
// 				break;
// 		}
// 	}
// 	return cur_mode_type;
// }

// // TODO: This is currently always returning 0. Bug?
// int NoteLogic::generateRandomOctave() {
// 	double rand_octave_gen = RandomGenerator::randomFloat();
// 	double cur_octave_threshold = 0.0;
// 	int cur_octave_type = MIN_OCTAVE - 1;

// 	while (rand_octave_gen > cur_octave_threshold) {
// 		cur_octave_type++;
// 		switch (cur_octave_type) {
// 			case 3:
// 				cur_octave_threshold += OCTAVE_3_FREQ;
// 				break;
// 			case 4:
// 				cur_octave_threshold += OCTAVE_4_FREQ;
// 				break;
// 			case 5:
// 				cur_octave_threshold += OCTAVE_5_FREQ;
// 				break;
// 			case 6:
// 				cur_octave_threshold += OCTAVE_6_FREQ;
// 				break;
// 			case 7:
// 				cur_octave_threshold += OCTAVE_7_FREQ;
// 				break;
// 		}
// 	}
// 	return cur_octave_threshold;
// }


// int NoteLogic::getModeStep(int mode_type) {
// 	switch (mode_type) {
// 		case MODE_IONIAN:
// 			return MODE_STEP_IONIAN;
// 		case MODE_DORIAN:
// 			return MODE_STEP_DORIAN;
// 		case MODE_PHRYGIAN:
// 			return MODE_STEP_PHRYGIAN;
// 		case MODE_LYDIAN:
// 			return MODE_STEP_LYDIAN;
// 		case MODE_MIXOLYDIAN:
// 			return MODE_STEP_MIXOLYDIAN;
// 		case MODE_AEOLIAN:
// 			return MODE_STEP_AEOLIAN;
// 		case MODE_LOCRIAN:
// 			return MODE_STEP_LOCRIAN;
// 		default:
// 			return -1;
// 	}
// }


// string NoteLogic::getChordNameForNotes(vector<Note *> notes) {
// 	// TODO: implement this
// 	return string("A#");
// }

int NoteLogic::getMIDIKeyForNote(Note note) {
	return MIDI_ROOT_KEY + note.semitone;
}