#include <fstream>
#include <sstream>
#include <iostream>
#include <iterator>
#include "AreaDefinition.h"
#include "RandomGenerator.h"
using namespace std;

#define AREA_DEFINITION_FILENAME "AreaDefinitions/BasicMelodicDefinition.txt"

std::vector<AreaDefinition *> AreaDefinition::all_area_definitions;
std::map<std::string, struct Chord> AreaDefinition::all_chord_definitions;
std::map<std::string, struct ChordProgression> AreaDefinition::all_chord_progression_definitions;


AreaDefinition::AreaDefinition() {
	// TODO: any additional initialization
}


void AreaDefinition::initFromFile() {
	string filename = AREA_DEFINITION_FILENAME;
	ifstream input_stream(filename);
	string line;
	LineReadingMode lrm = NOT_AREA;
	AreaDefinition *current_area_definition = NULL;

	while (getline(input_stream, line)) {
		if (line.length() == 0) {
			continue;
		}
		current_area_definition = readLineFromFile(line, lrm, current_area_definition);
	}
	all_area_definitions.push_back(current_area_definition);
}


AreaDefinition *AreaDefinition::getRandomAreaDefinition() {
	int random_index = RandomGenerator::randomInt(0, all_area_definitions.size() - 1);
	return all_area_definitions[random_index];
}

AreaDefinition *AreaDefinition::getRandomCompatibleAreaDefinition(
	Direction dir,
	int prev_room_relative_x,
	int prev_room_relative_y
) {
	vector<AreaDefinition *> compatible_area_definitions;
	for (int i = 0; i < all_area_definitions.size(); i++) {
		// add compatible area defs to list
		if (areaCompatible(all_area_definitions[i], dir, prev_room_relative_x, prev_room_relative_y)) {
			compatible_area_definitions.push_back(all_area_definitions[i]);
		}
	}
	
	if (compatible_area_definitions.size() == 0) {
		return NULL;
	}
	int random_index = RandomGenerator::randomInt(0, compatible_area_definitions.size() - 1);
	return compatible_area_definitions[random_index];
}


bool AreaDefinition::areaCompatible(
	AreaDefinition *area_def, 
	Direction dir,
	int prev_area_def_room_relative_x,
	int prev_area_def_room_relative_y
) {
	int next_area_def_room_relative_x = prev_area_def_room_relative_x;
	int next_area_def_room_relative_y = prev_area_def_room_relative_y;

	switch (dir) {
		case NORTH:
			next_area_def_room_relative_y = 0;
			break;
		case SOUTH:
			next_area_def_room_relative_y = GRID_DIM_SIZE - 1;
			break;
		case EAST:
			next_area_def_room_relative_x = 0;
			break;
		case WEST:
			next_area_def_room_relative_x = GRID_DIM_SIZE - 1;
			break;
	}
	return area_def->roomExists(next_area_def_room_relative_x, next_area_def_room_relative_y);
}


bool AreaDefinition::areaSidesCompatible(AreaDefinition *area_def_a, AreaDefinition *area_def_b, Direction dir) {
	int area_def_a_room_relative_x = 0;
	int area_def_a_room_relative_y = 0;
	int area_def_b_room_relative_x = 0;
	int area_def_b_room_relative_y = 0;

	bool areas_compatible = true;

	switch (dir) {
		case NORTH:
			area_def_a_room_relative_y = GRID_DIM_SIZE - 1;
			break;
		case SOUTH:
			area_def_b_room_relative_y = GRID_DIM_SIZE - 1;
			break;
		case EAST:
			area_def_a_room_relative_x = GRID_DIM_SIZE - 1;
			break;
		case WEST:
			area_def_b_room_relative_x = GRID_DIM_SIZE - 1;
			break;
	}

	for (int i = 0; i < GRID_DIM_SIZE; i++) {
		switch (dir) {
			case NORTH:
			case SOUTH:
				area_def_a_room_relative_x = area_def_b_room_relative_x = i;
				break;
			case EAST:
			case WEST:
				area_def_a_room_relative_y = area_def_b_room_relative_y = i;
				break;
		}
		bool area_def_a_room_exists = area_def_a->roomExists(area_def_a_room_relative_x, area_def_a_room_relative_y);
		bool area_def_b_room_exists = area_def_b->roomExists(area_def_b_room_relative_x, area_def_b_room_relative_y);
		
		if (area_def_a_room_exists && (!area_def_b_room_exists)) {
			areas_compatible = false;
		}
		if ((!area_def_a_room_exists) && area_def_b_room_exists) {
			areas_compatible = false;
		}
	}
	return areas_compatible;
}


AreaDefinition *AreaDefinition::getAreaDefinition(int index) {
	return all_area_definitions[index];
}


bool AreaDefinition::roomExists(int room_x, int room_y) {
	return room_exists[room_x][room_y];
}

struct Chord AreaDefinition::getInitialRoomChordDefinition(int area_relative_x, int area_relative_y) {
	return initial_room_chord_definitions[area_relative_x][area_relative_y];
}

// struct Chord AreaDefinition::getCurrentRoomChordDefinitions(int area_relative_x, int area_relative_y) {
// 	return current_room_chord_definitions[area_relative_x][area_relative_y];
// }


AreaDefinition *AreaDefinition::readLineFromFile(string line, LineReadingMode &lrm, AreaDefinition *current_area_definition) {
	int cell_y = 0;
	switch (lrm) {
		case AREA_FIRST_ROW:  // cell_y = 2;
			cell_y++;
		case AREA_SECOND_ROW: // cell_y = 1;
			cell_y++;
		case AREA_THIRD_ROW:  // cell_y = 0;
			lrm = current_area_definition->processAreaCellLine(line, cell_y);
			break;
		case AREA_SHUFFLE:
			current_area_definition->setShuffle(line);
			lrm = NOT_AREA;
		case NOT_AREA:
			current_area_definition = processNonAreaLine(line, lrm, current_area_definition);
			break;
		default:
			break;
	}
	return current_area_definition;
}


LineReadingMode AreaDefinition::processAreaCellLine(string line, int cell_y) {
	stringstream column_reader(line);
	string cell_notation = "";

	int cell_x = 0;
	while (getline(column_reader, cell_notation, '\t')) {
		bool read_success = processAreaCellNotation(cell_notation, cell_x, cell_y);
		if (!read_success) {
			cout << "Invalid area cell definition line: " << line << endl;
		}
		cell_x++;
	}

	if (cell_y == 2) {
		return AREA_SECOND_ROW;
	} else if (cell_y == 1) {
		return AREA_THIRD_ROW;
	} else {
		return AREA_SHUFFLE;
	}
}


AreaDefinition *AreaDefinition::processNonAreaLine(string line, LineReadingMode &lrm, AreaDefinition *current_area_definition) {
	if (line == "CELL") {
		if (current_area_definition != NULL) {
			all_area_definitions.push_back(current_area_definition);
		}
		current_area_definition = new AreaDefinition();
		// TODO: fix fencepost
		lrm = AREA_FIRST_ROW;
	}


	// check for chord definitions
	string chord_prefix = "CHORD";
	if (line.substr(0, chord_prefix.length()) == chord_prefix) {
		stringstream column_reader(line.substr(chord_prefix.length() + 1));
		string chord_name;
		string semitone_str;

		// get chord name
		getline(column_reader, chord_name, '\t');

		// read semitones into chord
		struct Chord chord;
		while (getline(column_reader, semitone_str, '\t')) {
			Note note;
			note.semitone = stoi(semitone_str);
			chord.notes.push_back(note);
		}
		all_chord_definitions[chord_name] = chord;
		lrm = NOT_AREA;
	}
	// TODO: check for progression definitions
	return current_area_definition;
}


bool AreaDefinition::processAreaCellNotation(string cell_notation, int cell_x, int cell_y) {
	if (cell_notation == "N") {
		room_exists[cell_x][cell_y] = false;
		return true;
	}

	// from here on out, we assume the room exists;
	room_exists[cell_x][cell_y] = true;

	if (cell_notation == "R") {
		int random_chord_index = RandomGenerator::randomInt(0, all_chord_definitions.size() - 1);

		// this pushes an iterator to a random index in the map
		map<string, struct Chord>::iterator it = all_chord_definitions.begin();
		advance(it, random_chord_index);

		// it->second is the chord value
		initial_room_chord_definitions[cell_x][cell_y] = it->second;
		// current_room_chord_definitions[cell_x][cell_y] = it->second;
		return true;
	}

	if (cell_notation == "E") {
		struct Chord empty_chord;
		initial_room_chord_definitions[cell_x][cell_y] = empty_chord;
		// current_room_chord_definitions[cell_x][cell_y] = empty_chord;
		return true;
	}

	if (all_chord_definitions.find(cell_notation) != all_chord_definitions.end()) {
		struct Chord specific_chord = all_chord_definitions[cell_notation];
		initial_room_chord_definitions[cell_x][cell_y] = specific_chord;
		// current_room_chord_definitions[cell_x][cell_y] = specific_chord;
		return true;
	}

	if (all_chord_progression_definitions.find(cell_notation) != all_chord_progression_definitions.end()) {
		// TODO: handle chord progression
		return true;
	}

	return false;
}


void AreaDefinition::setShuffle(string line) {
	if (line != "SHUFFLE") {
		cout << "HUH" << endl;
		this->can_shuffle = false;
	} else {
		this->can_shuffle = true;
	}
}


bool AreaDefinition::canShuffle() {
	return can_shuffle;
}


// void AreaDefinition::shuffleNotesAcrossRooms(string line) {
// 	if (line != "T") {
// 		notes_shuffled_across_rooms = false;
// 		return;
// 	}

// 	notes_shuffled_across_rooms = true;
// 	vector<struct Note> notes_in_area;
	
// 	// add all notes from all chords to a list
// 	// also set all rooms to have empty current chords
// 	for (int cell_x = 0; cell_x < GRID_DIM_SIZE; cell_x++) {
// 		for (int cell_y = 0; cell_y < GRID_DIM_SIZE; cell_y++) {
// 			if (room_exists[cell_x][cell_y]) {

// 				// inside an existing room
// 				struct Chord room_chord = initial_room_chord_definitions[cell_x][cell_y];
// 				for (int i = 0; i < room_chord.notes.size(); i++) {
// 					notes_in_area.push_back(room_chord.notes[i]);
// 				}
// 				struct Chord empty_chord;
// 				current_room_chord_definitions[cell_x][cell_y] = empty_chord;

// 			}
// 		}
// 	}

// 	// for each note, choose a random room for it to join
// 	for (int i = 0; i < notes_in_area.size(); i++) {
// 		int new_cell_x = RandomGenerator::randomInt(0, GRID_DIM_SIZE - 1);
// 		int new_cell_y = RandomGenerator::randomInt(0, GRID_DIM_SIZE - 1);
// 		current_room_chord_definitions[new_cell_x][new_cell_y].notes.push_back(notes_in_area[i]);
// 	}
// }