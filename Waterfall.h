#ifndef WATERFALL_H
#define WATERFALL_H

#include "NocturneConstants.h"
#include <deque>

// waterfall constants
#define NUM_BUFFERS_IN_WATERFALL 16

class Waterfall {
public:
	void pushBuffer(BufferWrapper buffer);
	std::deque<BufferWrapper> getAllBuffers();
private:
	std::deque<BufferWrapper> waterfall_buffers;
};

#endif