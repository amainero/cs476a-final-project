#include "Resident.h"
#include <string>
#include <map>

#ifndef NPC_H
#define NPC_H

using namespace std;

class NocturneNPC : public Resident {
public:
	NocturneNPC(string id, NocturneAilment ailment, double color[3]);
	~NocturneNPC();

	void cure();

	string getID();
	static NocturneNPC* getNPC(string id);
	virtual bool isNPC() { return true; }
private:
	string id;
	static map<string, NocturneNPC*> npcMap;
};

#endif