#include "NocturnePerson.h"
#include "Board.h"
#include <iostream>

#define COLLISION_THRESHOLD (0.001 * PERSON_RADIUS)

using namespace std;

NocturnePerson::NocturnePerson() {
	x_vel = 0.0;
	y_vel = 0.0;

	/*accel_strength = 0.0;
	accel_dir[0] = 0.0;
	accel_dir[1] = 0.0;*/
	x_accel = 0.0;
	y_accel = 0.0;

	sanity = 1.0;

	oldRoom = NULL;

	color[0] = 1.0;
	color[1] = 1.0;
	color[2] = 1.0;
	restingColor[0] = 1.0;
	restingColor[1] = 1.0;
	restingColor[2] = 1.0;
	colorFadeSpeed = COLOR_FADE_SPEED;

	collisionsEnabled = true;
	canChangeRooms = false;
	canGenerateArea = false;

	radius = PERSON_RADIUS;
	myAilment = NO_AILMENT;
}

// May be used to have a variable radius later.
double NocturnePerson::getRadius() {
	return radius;
}

double NocturnePerson::getSanity() { return sanity; }

NocturneAilment NocturnePerson::ailment() { return myAilment; }


void NocturnePerson::setLocation(GLfloat absolute_x, GLfloat absolute_y) {
	x_loc = absolute_x;
	y_loc = absolute_y;
}

void NocturnePerson::setVel(double x_vel, double y_vel) {
	this->x_vel = x_vel;
	this->y_vel = y_vel;
}


void NocturnePerson::draw() {
	glColor3f(color[0], color[1], color[2]);
	glBegin( GL_TRIANGLE_FAN );
    for (int j = 0; j < NUM_PERSON_POINTS; j++) {
        GLfloat x_pos = x_loc + getRadius() * ::cos(TWOPI * j / (double) NUM_PERSON_POINTS);
        GLfloat y_pos = y_loc + getRadius() * ::sin(TWOPI * j / (double) NUM_PERSON_POINTS);
        glVertex2f(x_pos, y_pos);
    }
    glEnd();
}

double sign(double val) {
	if (val < 0) return -1;
	if (val > 0) return 1;
	return 0;
}

void accelAndDecel(double y_accel, double x_accel, double &y_vel, double &x_vel, double timeChange) {
	// Decel.
	// Find accel bug
	// FIX: Accel / decel MAGNITUDE (speed) rather than COMPONENTS (velocity)
	//if (accel_strength == 0) {
	if (y_accel == 0) {
		if (y_vel > 0) y_vel -= min(y_vel, timeChange * ACCEL_PER_SECOND);
		if (y_vel < 0) y_vel -= max(y_vel, timeChange * ACCEL_PER_SECOND * -1);
		//if (y_vel > 0) y_vel -= min(y_vel, timeChange * 0.2);
		//if (y_vel < 0) y_vel -= max(y_vel, timeChange * 0.2 * -1);
	}
	//if (accel_strength == 0) {
	if (x_accel == 0) {
		if (x_vel > 0) x_vel -= min(x_vel, timeChange * ACCEL_PER_SECOND);
		if (x_vel < 0) x_vel -= max(x_vel, timeChange * ACCEL_PER_SECOND * -1);
		//if (x_vel > 0) x_vel -= min(x_vel, timeChange * 0.2);
		//if (x_vel < 0) x_vel -= max(x_vel, timeChange * 0.2 * -1);
	}

	// Accel.
	/*if (accel_strength > 0) {
		double angle;
		if (accel_dir[0] == 0.0) angle = (PI / 2.0) * sign(accel_dir[1]);
		else double angle = atan(accel_dir[1] / accel_dir[0]);
		if (accel_dir[0] < 0) angle += PI;

		y_vel += sin(angle) * accel_strength;
		x_vel += cos(angle) * accel_strength;
	}*/

	if (y_accel) y_vel += timeChange * y_accel;
	if (x_accel) x_vel += timeChange * x_accel;
}

void NocturnePerson::computeBehavior(double timeLapsed) {
	// Fade color
	for (int i = 0; i < 3; i++) {
		if (color[i] > restingColor[i]) color[i] = max(restingColor[i], color[i] - timeLapsed * colorFadeSpeed);
		if (color[i] < restingColor[i]) color[i] = min(restingColor[i], color[i] + timeLapsed * colorFadeSpeed);
	}

	accelAndDecel(y_accel, x_accel, y_vel, x_vel, timeLapsed);

	// Cap vel.
	double trueVel = sqrt(x_vel * x_vel + y_vel * y_vel);
	double maxVel = MAX_VELOCITY;
	if (ailment() == DEPRESSION) maxVel = DEPRESSED_VELOCITY + (MAX_VELOCITY - DEPRESSED_VELOCITY) * sanity;
	if (trueVel > maxVel) {
		x_vel *= maxVel / trueVel;
		y_vel *= maxVel / trueVel;
	}

	// Behavior
	NocturneObject::computeBehavior(timeLapsed);
	checkAndHandleWallCollision();

	// Update
	x_loc += x_vel * timeLapsed;
	y_loc += y_vel * timeLapsed;
	
	updateCurrentRoom();
}


void NocturnePerson::updateCurrentRoom() {
	Room *currentRoom = getRoom();
	if (currentRoom != oldRoom) {
		onRoomChange(currentRoom);
		oldRoom = currentRoom;
	}
}


void NocturnePerson::onRoomChange(Room* newRoom) {
	// Do nothing...
}


void NocturnePerson::collisionBehavior(NocturnePerson* otherPerson, double collisionStrength) {
	// Do nothing...
}

bool NocturnePerson::resolveCollision(NocturnePerson *otherPerson) {
	double dist = sqrt(
		(x_loc - otherPerson->x_loc) * (x_loc - otherPerson->x_loc) + 
		(y_loc - otherPerson->y_loc) * (y_loc - otherPerson->y_loc)
	);
	if (dist <= COLLISION_THRESHOLD) {
		x_loc += COLLISION_THRESHOLD;
		dist = sqrt(
			(x_loc - otherPerson->x_loc) * (x_loc - otherPerson->x_loc) + 
			(y_loc - otherPerson->y_loc) * (y_loc - otherPerson->y_loc)
		);
	}
	double minDist = getRadius() + otherPerson->getRadius();
	if (dist > minDist) return false;

	// WE COLLIDED! CORRECT! CORRECT!
	double distToRepel = (minDist - dist + COLLISION_FP_FIX) / 2.0;
	static int num = 0;

	double angleVec[2];
	angleVec[0] = x_loc - otherPerson->x_loc;
	angleVec[1] = y_loc - otherPerson->y_loc;

	double angle;
	if (angleVec[0] == 0.0) angle = (angleVec[1] > 0) ? 90 : -90;
	else {
		angle = atan(angleVec[1] / angleVec[0]);
		if (angleVec[0] < 0.0) angle += PI;
	}

	double x_repel = distToRepel * cos(angle);
	double y_repel = distToRepel * sin(angle);
	//double x_repel = distToRepel * ((x_loc - otherPerson->x_loc) / ((x_loc - otherPerson->x_loc) + (y_loc - otherPerson->y_loc)));
	//double y_repel = distToRepel * ((y_loc - otherPerson->y_loc) / ((x_loc - otherPerson->x_loc) + (y_loc - otherPerson->y_loc)));

	if (collisionsEnabled) {
		if (!otherPerson->collisionsEnabled) {
			x_repel *= 2.0;
			y_repel *= 2.0;
		}
		x_loc += x_repel;
		y_loc += y_repel;
	}

	if (otherPerson->collisionsEnabled) {
		if (!collisionsEnabled) {
			x_repel *= 2.0;
			y_repel *= 2.0;
		}
		otherPerson->x_loc -= x_repel;
		otherPerson->y_loc -= y_repel;
	}

	return true;
}

double* NocturnePerson::breakVelTowardsPoint(double x, double y) {
	double angleVec[2];
	angleVec[0] = x_loc - x;
	angleVec[1] = y_loc - y;

	double angle = -atan(angleVec[1] / angleVec[0]);

	double* newVec = new double[2];
	newVec[0] = x_vel * cos(angle) - y_vel * sin(angle);
	newVec[1] = x_vel * sin(angle) + y_vel * cos(angle);

	return newVec;
}

void NocturnePerson::assembleVelTowardsPoint(double x, double y, double* brokenVel) {
	double angleVec[2];
	angleVec[0] = x_loc - x;
	angleVec[1] = y_loc - y;

	double angle = atan(angleVec[1] / angleVec[0]);

	x_vel = brokenVel[0] * cos(angle) - brokenVel[1] * sin(angle);
	y_vel = brokenVel[0] * sin(angle) + brokenVel[1] * cos(angle);
}

void NocturnePerson::checkAndHandleWallCollision() {
	if (!collisionsEnabled) return;

	double northY = floor((y_loc + ROOM_OFFSET) / ROOM_SIZE) * ROOM_SIZE + ROOM_OFFSET;
	double southY = (floor((y_loc + ROOM_OFFSET) / ROOM_SIZE) - 1) * ROOM_SIZE + ROOM_OFFSET;
	double eastX = floor((x_loc + ROOM_OFFSET) / ROOM_SIZE) * ROOM_SIZE + ROOM_OFFSET;
	double westX = (floor((x_loc + ROOM_OFFSET) / ROOM_SIZE) - 1) * ROOM_SIZE + ROOM_OFFSET;

	Room* myRoom = getRoom();
	int roomGridX = NocturneObject::theBoard->getGridCoordinateFromRawCoordinateX(x_loc);
	int roomGridY = NocturneObject::theBoard->getGridCoordinateFromRawCoordinateY(y_loc);

	// North
	if (y_loc + getRadius() > northY) {
		bool collisionOccurred = true;
		bool doorExists = canChangeRooms && myRoom->wallIsOpen(NORTH);
		if (!canGenerateArea &&
			NocturneObject::theBoard->getRoomFromGridCoordinates(
				roomGridX,
				roomGridY + 1) == NULL) {
			doorExists = false;
		}
		double doorWallSize = (ROOM_SIZE - DOOR_SIZE) / 2.0;
		if (doorExists &&
			(x_loc - getRadius() > westX + doorWallSize) &&
			(x_loc + getRadius() < eastX - doorWallSize)) {
			collisionOccurred = false;
		}
		if (collisionOccurred) {
			y_loc = northY - getRadius() - COLLISION_FP_FIX;
			if (y_vel > 0.0) y_vel *= -WALL_ELASTICITY;
		}
	}

	// South
	if (y_loc - getRadius() < southY) {
		bool collisionOccurred = true;
		bool doorExists = canChangeRooms && myRoom->wallIsOpen(SOUTH);
		if (!canGenerateArea &&
			NocturneObject::theBoard->getRoomFromGridCoordinates(
				roomGridX,
				roomGridY - 1) == NULL) {
			doorExists = false;
		}
		double doorWallSize = (ROOM_SIZE - DOOR_SIZE) / 2.0;
		if (doorExists &&
			(x_loc - getRadius() > westX + doorWallSize) &&
			(x_loc + getRadius() < eastX - doorWallSize)) {
			collisionOccurred = false;
		}
		if (collisionOccurred) {
			y_loc = southY + getRadius() + COLLISION_FP_FIX;
			if (y_vel < 0.0) y_vel *= -WALL_ELASTICITY;
		}
	}

	// East
	if (x_loc + getRadius() > eastX) {
		bool collisionOccurred = true;
		bool doorExists = canChangeRooms && myRoom->wallIsOpen(EAST);
		if (!canGenerateArea &&
			NocturneObject::theBoard->getRoomFromGridCoordinates(
				roomGridX + 1,
				roomGridY) == NULL) {
			doorExists = false;
		}
		double doorWallSize = (ROOM_SIZE - DOOR_SIZE) / 2.0;
		if (doorExists &&
			(y_loc - getRadius() > southY + doorWallSize) &&
			(y_loc + getRadius() < northY - doorWallSize)) {
			collisionOccurred = false;
		}
		if (collisionOccurred) {
			x_loc = eastX - getRadius() - COLLISION_FP_FIX;
			if (x_vel > 0.0) x_vel *= -WALL_ELASTICITY;
		}
	}

	// West
	if (x_loc - getRadius() < westX) {
		bool collisionOccurred = true;
		bool doorExists = canChangeRooms && myRoom->wallIsOpen(WEST);
		if (!canGenerateArea &&
			NocturneObject::theBoard->getRoomFromGridCoordinates(
				roomGridX - 1,
				roomGridY) == NULL) {
			doorExists = false;
		}
		double doorWallSize = (ROOM_SIZE - DOOR_SIZE) / 2.0;
		if (doorExists &&
			(y_loc - getRadius() > southY + doorWallSize) &&
			(y_loc + getRadius() < northY - doorWallSize)) {
			collisionOccurred = false;
		}
		if (collisionOccurred) {
			x_loc = westX + getRadius() + COLLISION_FP_FIX;
			if (x_vel < 0.0) x_vel *= -WALL_ELASTICITY;
		}
	}
}

void NocturnePerson::checkAndHandleCollision(NocturnePerson* otherPerson) {
	if (otherPerson == this) return;

	if (resolveCollision(otherPerson)) {
		double* myBrokenVel = breakVelTowardsPoint(otherPerson->x_loc, otherPerson->y_loc);
		double* theirBrokenVel = otherPerson->breakVelTowardsPoint(x_loc, y_loc);

		if (collisionsEnabled) collisionBehavior(otherPerson, fabs(theirBrokenVel[0] - myBrokenVel[0]));
		if (otherPerson->collisionsEnabled) otherPerson->collisionBehavior(this, fabs(theirBrokenVel[0] - myBrokenVel[0]));

		double temp = myBrokenVel[0];
		myBrokenVel[0] = theirBrokenVel[0];
		theirBrokenVel[0] = temp;

		if (collisionsEnabled) assembleVelTowardsPoint(otherPerson->x_loc, otherPerson->y_loc, myBrokenVel);
		if (otherPerson->collisionsEnabled) otherPerson->assembleVelTowardsPoint(x_loc, y_loc, theirBrokenVel);

		delete myBrokenVel;
		delete theirBrokenVel;
	}
}

double NocturnePerson::stopX() {
	double timeToStop = fabs(x_vel / ACCEL_PER_SECOND);
	return x_loc + (x_vel / 2.0) * timeToStop;
}
double NocturnePerson::stopY() {
	double timeToStop = fabs(y_vel / ACCEL_PER_SECOND);
	return y_loc + (y_vel / 2.0) * timeToStop;
}




