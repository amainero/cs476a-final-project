#include "NocturneMessage.h"
#include "Keeper.h"
#include "NocturneNPC.h"
#include <map>
#include <fstream>
#include <iostream>

using namespace std;

vector<NocturneMessage*> NocturneMessage::activeMessages = vector<NocturneMessage*>();
NocturneCamera* NocturneMessage::theCamera = NULL;;

void NocturneMessage::chatter(double x, double y, string message, double timeIntoChatter) {
	if (timeIntoChatter > CHATTER_TIME) return;

	int location[2];
	NocturneObject::getScreenCoordsForPoint(x, y + CHATTER_OFFSET, location);

	float alpha = 1.0;
	if (timeIntoChatter < DEFAULT_FADE_TIME) alpha = timeIntoChatter / DEFAULT_FADE_TIME;
	if (CHATTER_TIME - timeIntoChatter < DEFAULT_FADE_TIME) alpha = (CHATTER_TIME - timeIntoChatter) / DEFAULT_FADE_TIME;

	glColor4f(1.0, 1.0, 1.0, alpha);
	int xPixel = location[0] - NocturneMessage::widthOfMessage(GLUT_BITMAP_HELVETICA_12, message) / 2;
	int yPixel = location[1];
	glWindowPos2i(xPixel, yPixel);
	for (int i = 0; i < message.length(); i++) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, message[i]);
	}
}

NocturneDialog::NocturneDialog(string filename) {
	ifstream input(filename);
	string line;
	getline(input, line);
	id = line;
	map<string, NocturneMessage*> messageMap;

	NocturneMessage *currentMessage = NULL;
	int currentMessageLine = 0;
	string currentMessageID = "";
	string currentMessageContent = "";
	void *currentFont = NULL;
	double currentDuration = 0.0;
	double currentXFrac = 0.0;
	double currentYFrac = 0.0;
	string prevMessageID = "";
	double currentDelay = 0.0;
	bool isInFinish = false;
	int finishLine = 0;
	double finishXFrac = 0.0;
	double finishYFrac = 0.0;
	double finishTime = 0.0;

	bool parsingExemptions = false;

	while(getline(input, line)) {
		if (line.length() == 0) {
			continue;
		}
		if (line == "EXEMPT") {
			parsingExemptions = true;
			continue;
		}
		if (parsingExemptions) {
			exemptions.push_back(line);
		}
		if (line == "MESSAGE") {
			// Commit currentMessage
			if (currentMessageID != "") {
				currentMessage = new NocturneMessage(currentMessageContent, currentFont, currentDuration, currentXFrac, currentYFrac);
				double prevTime = (prevMessageID == "none") ? 0.0 : messageMap[prevMessageID]->totalTime();
				currentMessage->setDelay(prevTime + currentDelay);
				if (isInFinish) {
					currentMessage->setFinishLoc(finishXFrac, finishYFrac);
					if (finishTime != 0.0) currentMessage->setMoveTime(finishTime);
				}
				currentMessage->setSelfDestruct(false);
				messageMap[currentMessageID] = currentMessage;
				messages.push_back(currentMessage);
			}

			currentMessageLine = 0;
			isInFinish = false;
			parsingExemptions = false;
		} else if (line == "FINISH") {
			finishLine = 0;
			parsingExemptions = false;
			isInFinish = true;
			finishTime = 0.0;
		} else {
			if (!isInFinish) {
				switch(currentMessageLine) {
					case 0:
					currentMessageID = line;
					break;

					case 1:
					currentMessageContent = line;
					break;

					case 2:
					if (line == "big") currentFont = GLUT_BITMAP_HELVETICA_18;
					else currentFont = GLUT_BITMAP_HELVETICA_12;
					break;

					case 3:
					currentDuration = atof(line.c_str());
					break;

					case 4:
					currentXFrac = atof(line.c_str());
					break;

					case 5:
					currentYFrac = atof(line.c_str());
					break;

					case 6:
					prevMessageID = line;
					break;

					case 7:
					currentDelay = atof(line.c_str());
					break;
				}
				currentMessageLine++;
			} else {
				switch(finishLine) {
					case 0:
					finishXFrac = atof(line.c_str());
					break;

					case 1:
					finishYFrac = atof(line.c_str());
					break;

					case 2:
					finishTime = atof(line.c_str());
					break;
				}
				finishLine++;
			}
		}
	}

	// Commit currentMessage
	if (currentMessageID != "") {
		currentMessage = new NocturneMessage(currentMessageContent, currentFont, currentDuration, currentXFrac, currentYFrac);
		double prevTime = (prevMessageID == "none") ? 0.0 : messageMap[prevMessageID]->totalTime();
		currentMessage->setDelay(prevTime + currentDelay);
		if (isInFinish) {
			currentMessage->setFinishLoc(finishXFrac, finishYFrac);
			if (finishTime != 0.0) currentMessage->setMoveTime(finishTime);
		}
		currentMessage->setSelfDestruct(false);
		messageMap[currentMessageID] = currentMessage;
		messages.push_back(currentMessage);
	}
}

NocturneDialog::~NocturneDialog() {
	for (int i = 0; i < messages.size(); i++) delete messages[i];
}

double NocturneDialog::play() {
	for (int i = 0; i < messages.size(); i++) {
		messages[i]->play();
	}
	NocturneMessage::theCamera->fadeOutWithSpeed(1.0);
	NocturneMessage::theCamera->fadeUpAfterDelayWithSpeed(messages[messages.size() - 1]->totalTime(), 1.0);
	for (int i = 0; i < exemptions.size(); i++) {
		if (exemptions[i] == "KEEPER") NocturneMessage::theCamera->makeExempt(Keeper::theKeeper);
		else {
			NocturnePerson *person = NocturneNPC::getNPC(exemptions[i]);
			if (person) NocturneMessage::theCamera->makeExempt(person);
		}
	}
	return messages[messages.size() - 1]->totalTime();
}

string NocturneDialog::getID() { return id; }

double NocturneMessage::intro() {
	/*NocturneMessage* first = new NocturneMessage(string("You awaken to the sound of voices in the darkness."), GLUT_BITMAP_HELVETICA_12, 3.0, 0.5, 0.5);
    first->setDelay(2.0);
    first->play();

    NocturneMessage* second = new NocturneMessage(string("\"Welcome to The Nocturne\""), GLUT_BITMAP_HELVETICA_18, 3.0, 0.5, 0.5);
    second->setDelay(first->totalTime() + 0.5);
    second->play();

    NocturneMessage* third = new NocturneMessage(string("What is this place?"), GLUT_BITMAP_HELVETICA_12, 7.0, 0.5, 0.5);
    third->setDelay(second->totalTime() + 0.5);
    third->play();

    NocturneMessage* thoughtOne = new NocturneMessage(string("A dream?"), GLUT_BITMAP_HELVETICA_12, 4.0, 0.25, 0.75);
    thoughtOne->setDelay(second->totalTime() + 2.0 + 0.5);
    thoughtOne->setFinishLoc(0.2, 0.8);
    thoughtOne->play();

    NocturneMessage* thoughtTwo = new NocturneMessage(string("An afterlife?"), GLUT_BITMAP_HELVETICA_12, 4.0, 0.7, 0.6);
    thoughtTwo->setDelay(second->totalTime() + 4.0 + 0.5);
    thoughtTwo->setFinishLoc(0.75, 0.6);
    thoughtTwo->play();

    NocturneMessage* thoughtThree = new NocturneMessage(string("A hallucination?"), GLUT_BITMAP_HELVETICA_18, 4.0, 0.5, 0.25);
    thoughtThree->setDelay(second->totalTime() + 6.0 + 0.5);
    thoughtThree->setFinishLoc(0.5, 0.2);
    thoughtThree->play();

    NocturneMessage* fourth = new NocturneMessage(string("Whatever it is,"), GLUT_BITMAP_HELVETICA_12, 3.0, 0.5, 0.55);
    fourth->setDelay(thoughtThree->totalTime() + 1.0);
    fourth->play();

    NocturneMessage* fifth = new NocturneMessage(string("it belongs to you now..."), GLUT_BITMAP_HELVETICA_12, 3.0, 0.5, 0.45);
    fifth->setDelay(thoughtThree->totalTime() + 1.0 + 1.5);
    fifth->play();

    NocturneMessage* sixth = new NocturneMessage(string("And you to it."), GLUT_BITMAP_HELVETICA_18, 3.0, 0.5, 0.5);
    sixth->setDelay(fifth->totalTime() + 0.5);
    sixth->play();

    NocturneMessage* last = new NocturneMessage(string("Slowly, your eyes adjust to your surroundings..."), GLUT_BITMAP_HELVETICA_12, 7.0, 0.5, 0.9);
    last->setDelay(sixth->totalTime() + 1.0);
    last->play();

    return last->totalTime();*/

    NocturneDialog* intro = new NocturneDialog("./Assets/DialogDefinitions/intro.txt");
    double toReturn = intro->play();
    //delete intro;
    return toReturn;
}

void NocturneMessage::computeFrame() {
	double theTime = currentTime();
	for (int i = 0; i < NocturneMessage::activeMessages.size(); i++) {
		if (NocturneMessage::activeMessages[i]->updateAndCheckComplete(theTime)) {
			// Erase the message.
			NocturneMessage* theMessage = NocturneMessage::activeMessages[i];
			NocturneMessage::activeMessages.erase(NocturneMessage::activeMessages.begin() + i);
			i--;
			if (theMessage->thisMessageWillSelfDestruct) delete theMessage;
		}
	}
}

NocturneMessage::NocturneMessage(string message, void* font, double duration, double xFrac, double yFrac) {
	this->message = message;
	this->font = font;
	this->delay = 0.0;
	this->fadeIn = DEFAULT_FADE_TIME;
	this->fadeOut = DEFAULT_FADE_TIME;
	this->sustain = duration;
	this->moveTime = sustain + fadeIn + fadeOut;
	this->xFracStart = xFrac;
	this->yFracStart = yFrac;
	this->xFracEnd = xFrac;
	this->yFracEnd = yFrac;

	thisMessageWillSelfDestruct = true;
}

void NocturneMessage::setMoveTime(double moveTime) {
	this->moveTime = moveTime;
}

void NocturneMessage::setSelfDestruct(bool destruct) {
	thisMessageWillSelfDestruct = destruct;
}

void NocturneMessage::play() {
	this->startTime = currentTime();

	NocturneMessage::activeMessages.push_back(this);
}

void NocturneMessage::setDelay(double delay) {
	this->delay = delay;
}

void NocturneMessage::setFinishLoc(double xFrac, double yFrac) {
	this->xFracEnd = xFrac;
	this->yFracEnd = yFrac;
}

int NocturneMessage::widthOfMessage(void* font, string message) {
	int toReturn = 0;
	for (int i = 0; i < message.length(); i++) {
		toReturn += glutBitmapWidth(font, message[i]);
	}
	return toReturn;
}

int NocturneMessage::messageWidth() {
	return NocturneMessage::widthOfMessage(font, message);
}

double NocturneMessage::totalTime() {
	return delay + fadeIn + sustain + fadeOut;
}

bool NocturneMessage::updateAndCheckComplete(double theTime) {
	if (theTime - startTime <= delay) return false;
	double afterDelayTime = startTime + delay;
	double timeForAnimation = moveTime;
	double timeIntoAnimation = theTime - afterDelayTime;
	if (timeIntoAnimation > timeForAnimation) timeForAnimation = timeIntoAnimation;

	if (timeIntoAnimation > timeForAnimation) return true;

	if (NocturneMessage::theCamera == NULL) return false;

	double frameXFrac = xFracStart + (xFracEnd - xFracStart) * (timeIntoAnimation / timeForAnimation);
	double frameYFrac = yFracStart + (yFracEnd - yFracStart) * (timeIntoAnimation / timeForAnimation);
	int xPixel = (NocturneMessage::theCamera->windowWidth * frameXFrac) - (messageWidth() / 2);
	int yPixel = (NocturneMessage::theCamera->windowHeight * frameYFrac);
	float alpha = 1.0;

	if (timeIntoAnimation <= fadeIn) alpha = timeIntoAnimation / fadeIn;
	if (timeIntoAnimation >= fadeIn + sustain) alpha = 1.0 - (timeIntoAnimation - fadeIn - sustain) / fadeOut;

	glColor4f(1.0, 1.0, 1.0, alpha);
	glWindowPos2i(xPixel, yPixel);
	for (int i = 0; i < message.length(); i++) {
		glutBitmapCharacter(font, message[i]);
	}
	return false;
}