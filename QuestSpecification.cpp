#include "QuestSpecification.h"
#include "AssetManager.h"
#include "Room.h"
#include "Area.h"

#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

enum QuestSpecReadingMode {
	ID,
	QUEST_TYPE,
	AILMENT,
	NPCS,
	NPC_GROUPS,
	GENERAL_CHATTER,
	SEGMENTS,
	ROOM_UNLOCK_STAGE,
	SEGMENT_COMPLETION_CONDITIONS,
	SEGMENT_CHATTER
};

bool QuestSegmentCompletionCondition::isSatisfied() {
	if (condition_type == IS_ALONE) {
		NocturneNPC* npc = NocturneNPC::getNPC(npc_stub_id_1);
		if (!npc) return false;
		Room* room = npc->getRoom();
		return (room->getResidents().size() == 1);
	} else if (condition_type == IN_SAME_ROOM) {
		NocturneNPC* npc1 = NocturneNPC::getNPC(npc_stub_id_1);
		if (!npc1) return false;
		NocturneNPC* otherPerson = NocturneNPC::getNPC(npc_stub_id_2);
		if (!otherPerson) return false;
		Room* room = npc1->getRoom();
		vector<Resident*> roomPeeps = room->getResidents();
		for (int i = 0; i < roomPeeps.size(); i++) {
			if (roomPeeps[i] == otherPerson) {
				return true;
			}
		}
		return false;
	} else if (condition_type == IN_LOCKED_ROOM) {
		NocturneNPC* npc = NocturneNPC::getNPC(npc_stub_id_1);
		if (!npc) return false;
		Area* area = npc->getArea();
		return (npc->getRoom() == area->getLockedRoom());
	} else if (condition_type == HAS_ITEM) {
		// TODO: Integrate items into this.
		return false;
	}
	return false;
}

bool QuestSegment::isComplete() {
	for (int i = 0; i < conditions.size(); i++) {
		if (!conditions[i].isSatisfied()) {
			return false;
		}
	}
	return true;
}


QuestSpecification::QuestSpecification(string filename) {
	readFile(filename);
}


void QuestSpecification::readFile(string filename) {
	QuestSpecReadingMode reading_mode = ID;
	ifstream input_stream(filename);
	string line;

	string cur_segment_id = "";

	while (getline(input_stream, line)) {
		if (line.length() == 0) {
			continue;
		}

		if (line[0] == '#') {
			continue;
		}

		if (line == "ID") {
			reading_mode = ID;
			continue;
		} else if (line == "QUEST_TYPE") {
			reading_mode = QUEST_TYPE;
			continue;
		} else if (line == "AILMENT") {
			reading_mode = AILMENT;
			continue;
		} else if (line == "NPCS") {
			reading_mode = NPCS;
			continue;
		} else if (line == "NPC_GROUPS") {
			reading_mode = NPC_GROUPS;
			continue;
		} else if (line == "GENERAL_CHATTER") {
			reading_mode = GENERAL_CHATTER;
			continue;
		} else if (line == "SEGMENTS") {
			reading_mode = SEGMENTS;
			continue;
		} else if (line == "ROOM_UNLOCK_STAGE") {
			reading_mode = ROOM_UNLOCK_STAGE;
			continue;
		} else if (line == "SEGMENT_COMPLETION_CONDITIONS") {
			reading_mode = SEGMENT_COMPLETION_CONDITIONS;
			continue;
		} else if (line == "SEGMENT_CHATTER") {
			reading_mode = SEGMENT_CHATTER;
			continue;
		}

		switch(reading_mode) {
			case ID:
				readID(line);
				break;
			case QUEST_TYPE:
				readQuestType(line);
				break;
			case AILMENT:
				readAilment(line);
				break;
			case NPCS:
				readNPC(line);
				break;
			case NPC_GROUPS:
				readNPCGroup(line);
				break;
			case GENERAL_CHATTER:
				readGeneralChatter(line);
				break;
			case SEGMENTS:
				readSegment(line);
				break;
			case ROOM_UNLOCK_STAGE:
				readRoomUnlockStage(line);
				break;
			case SEGMENT_COMPLETION_CONDITIONS:
				readSegmentCompletionCondition(line);
				break;
			case SEGMENT_CHATTER:
				readSegmentChatter(line, cur_segment_id);
				break;
		}
	}
}


void QuestSpecification::readID(string line) {
	this->id = line;
}


void QuestSpecification::readQuestType(string line) {
	if (line == "RELATIONSHIP") {
		this->quest_type = RELATIONSHIP;
	} else if (line == "ITEM") {
		this->quest_type = ITEM;
	} else {
		cout << "Error reading quest type: " << line << endl;
	}
}


void QuestSpecification::readAilment(string line) {
	if (line == "DEPRESSION") {
		this->ailment_type = DEPRESSION;
	} else if (line == "AMNESIA") {
		this->ailment_type = AMNESIA;
	} else if (line == "SCHIZOPHRENIA") {
		this->ailment_type = SCHIZOPHRENIA;
	} else {
		cout << "Error reading ailment: " << line << endl;
	}
}


void QuestSpecification::readNPC(string line) {
	vector<string> npc_elems;
	AssetManager::split(line, npc_elems);

	if (npc_elems.size() < 3) {
		cout << "Error reading NPC: " << line << endl;
		return;
	}

	NocturneNPCStub npc_stub;

	npc_stub.id = npc_elems[0];
	npc_stub.note.semitone = stoi(npc_elems[1]);

	if (npc_elems[2] == "DEPRESSION") {
		npc_stub.ailment_type = DEPRESSION;
	} else if (npc_elems[2] == "AMNESIA") {
		npc_stub.ailment_type = AMNESIA;
	} else if (npc_elems[2] == "SCHIZOPHRENIA") {
		npc_stub.ailment_type = SCHIZOPHRENIA;
	} else if (npc_elems[2] == "NO_AILMENT") {
		npc_stub.ailment_type = NO_AILMENT;
	} else {
		cout << "Error reading NPC ailment: " << line << endl;
		return;
 	}

 	npc_stub.npcColor[0] = stof(npc_elems[3]);
 	npc_stub.npcColor[1] = stof(npc_elems[4]);
 	npc_stub.npcColor[2] = stof(npc_elems[5]);

 	this->npc_stubs.push_back(npc_stub);
}


void QuestSpecification::readNPCGroup(string line) {
	vector<string> npc_group_elems;
	AssetManager::split(line, npc_group_elems);

	NocturneNPCGroup npc_group;
	for (int i = 0; i < npc_group_elems.size(); i++) {
		npc_group.npc_id_list.push_back(npc_group_elems[i]);
	}
	npc_groups.push_back(npc_group);
}


void QuestSpecification::readGeneralChatter(string line) {
	this->general_chatter.push_back(line);
}


void QuestSpecification::readSegment(string line) {
	vector<string> segment_elems;
	AssetManager::split(line, segment_elems);

	if (segment_elems.size() != 2) {
		cout << "Error reading segment: " << line << endl;
		return;
	}

	QuestSegment segment;
	segment.id = segment_elems[0];
	segment.completion_dialogue_id = segment_elems[1];
	segment.is_room_unlock_stage = false;

	this->segments.push_back(segment);
}


void QuestSpecification::readRoomUnlockStage(string line) {
	if (line == "NONE") {
		// no room unlock stage
		requires_locked_room = false;
		return;
	}

	requires_locked_room = true;

	for (int i = 0; i < segments.size(); i++) {
		if (segments[i].id == line) {
			segments[i].is_room_unlock_stage = true;
			return;
		}
	}

	cout << "Error reading room unlock stage: " << line << endl;
}


void QuestSpecification::readSegmentCompletionCondition(string line) {
	vector<string> condition_elems;
	AssetManager::split(line, condition_elems);

	if (condition_elems.size() < 3) {
		cout << "Error reading completion condition: " << line << endl;
		return;
	}

	// get segment id
	int segment_index = -1;
	for (int i = 0; i < segments.size(); i++) {
		if (segments[i].id == condition_elems[0]) {
			segment_index = i;
			break;
		}
	}

	if (segment_index == -1) {
		cout << "Error reading completion condition: segment id not found: " << line << endl;
		return;
	}

	QuestSegmentCompletionCondition completion_condition;

	// figure out condition type
	if (condition_elems[1] == "IS_ALONE") {
		completion_condition.condition_type = IS_ALONE;
	} else if (condition_elems[1] == "IN_SAME_ROOM") {
		completion_condition.condition_type = IN_SAME_ROOM;
	} else if (condition_elems[1] == "IN_LOCKED_ROOM") {
		completion_condition.condition_type = IN_LOCKED_ROOM;
	} else if (condition_elems[1] == "HAS_ITEM") {
		completion_condition.condition_type = HAS_ITEM;
	} else {
		cout << "Error reading completion condition_type: " << line << endl;
		return;
	}

	// get conditions: NPCs/items
	switch (completion_condition.condition_type) {
		case IS_ALONE:
			completion_condition.npc_stub_id_1 = condition_elems[2];
			break;
		case IN_SAME_ROOM:
			completion_condition.npc_stub_id_1 = condition_elems[2];
			completion_condition.npc_stub_id_2 = condition_elems[3];
			break;
		case IN_LOCKED_ROOM:
			completion_condition.npc_stub_id_1 = condition_elems[2];
			break;
		case HAS_ITEM:
			completion_condition.npc_stub_id_1 = condition_elems[2];
			completion_condition.item_stub_id = condition_elems[3];
			break;
	}

	// TODO: there's no checking here
	this->segments[segment_index].conditions.push_back(completion_condition);
}


void QuestSpecification::readSegmentChatter(string line, string &cur_segment_id) {
	// check for new segment id
	for (int i = 0; i < segments.size(); i++) {
		if (segments[i].id == line) {
			cur_segment_id = segments[i].id;
			return;
		}
	}

	// not a segment id, add to current chatter
	if (cur_segment_id == "") {
		cout << "Error: no segment id sepecified: " << line << endl;
		return;
	}

	for (int i = 0; i < segments.size(); i++) {
		if (segments[i].id == cur_segment_id) {
			segments[i].chatter.push_back(line);
			return;
		}
	}

	cout << "Error reading segment chatter: " << line << endl;
}
