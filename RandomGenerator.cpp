#include <random>
#include "RandomGenerator.h"
using namespace std;

random_device rd;
mt19937 gen(rd());
uniform_real_distribution<double> real_dis(0,1);

double RandomGenerator::randomFloat() {
	return real_dis(gen);
}


int RandomGenerator::randomInt(int min, int max) {
	uniform_int_distribution<int> int_dis(min, max);
	return int_dis(gen);
}