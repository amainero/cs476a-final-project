#include "QuestSpecification.h"
#include "NocturneConstants.h"
#include <vector>
#include <string>

class Quest {
	public:

		Quest(QuestSpecification* spec);
		bool checkAdvance();
		bool shouldUnlockRoom();
		bool isComplete();
		std::vector<std::string> getChatter();
		NocturneAilment getAilment();

	private:

		QuestSpecification* spec;
		int currentSegment;
		
};