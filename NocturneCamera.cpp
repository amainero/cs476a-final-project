#include "NocturneCamera.h"
#include "Timing.h"
#include <iostream>

using namespace std;

NocturneCamera::NocturneCamera() {
	distance = 10.0;
	x_loc = 0.0;
	y_loc = 0.0;
	destX = 0.0;
	destY = 0.0;
	currentMoveCommand = NULL;
	darkStart = 0;
	darkEnd = 1;

	windowWidth = 1600;
	windowHeight = 1200;

	fadeStart = 0;
	fadeDelay = 0;
	isFadingUp = false;
	isFadingOut = false;

	happinessLevel = 1.0;

	exempt = new vector<NocturnePerson*>();
}

void NocturneCamera::fadeOutWithSpeed(double speed) {
	initFadeOutDarkStart = darkStart;
	initFadeOutDarkEnd = darkEnd;
	isFadingOut = true;
	fadeStart = currentTime();

	fadeOutTime = speed;
}
void NocturneCamera::fadeUpAfterDelayWithSpeed(double delay, double speed) {
	fadeDelay = delay;
	fadeStart = currentTime();
	isFadingUp = true;

	fadeUpTime = speed;
}

void NocturneCamera::fadeOut() {
	fadeOutWithSpeed(DARK_FADE_OUT_TIME);
}

void NocturneCamera::fadeUpAfterDelay(double delay) {
	fadeUpAfterDelayWithSpeed(delay, DARK_FADE_UP_TIME);
}

void NocturneCamera::setCamera() {

	if (Keeper::theKeeper) {
		if (FOLLOW_KEEPER) {
			x_loc = Keeper::theKeeper->getX();
			y_loc = Keeper::theKeeper->getY();
		} else {
			// TODO: Center on area keeper is in. :D
			bool changeDest = false;
			while (Keeper::theKeeper->getX() >= destX + WORLD_OFFSET) {
				changeDest = true;
				destX += AREA_SIZE;
			}
			while (Keeper::theKeeper->getX() < destX - WORLD_OFFSET) {
				changeDest = true;
				destX -= AREA_SIZE;
			}
			while (Keeper::theKeeper->getY() >= destY + WORLD_OFFSET) {
				changeDest = true;
				destY += AREA_SIZE;
			}
			while (Keeper::theKeeper->getY() < destY - WORLD_OFFSET) {
				changeDest = true;
				destY -= AREA_SIZE;
			}
			if (changeDest) commandToLoc(destX, destY, CAMERA_MOVE_SPEED, SINE);
		}
	}

	glLoadIdentity();
	glTranslated(-x_loc, -y_loc, -distance);
}

float distBetweenPoints(int* p1, int* p2) {
	return sqrt((p1[0]-p2[0])*(p1[0]-p2[0])+(p1[1]-p2[1])*(p1[1]-p2[1]));
	//return abs(p1[0]-p2[0]) + abs(p1[1]-p2[1]);
}

void NocturneCamera::applyDarkness() {

	if (isFadingOut) {
		double theTime = currentTime();
		double timeIntoFade = theTime - fadeStart;
		if (timeIntoFade >= fadeOutTime) {
			darkStart = 0;
			darkEnd = 1;
			isFadingOut = false;
		} else {
			darkStart = initFadeOutDarkStart * (1.0 - timeIntoFade / fadeOutTime);
			darkEnd = initFadeOutDarkEnd * (1.0 - timeIntoFade / fadeOutTime);
		}
	}
	if (isFadingUp) {
		double theTime = currentTime();
		if (theTime - fadeStart >= fadeDelay) {
			double timeIntoFade = theTime - fadeStart - fadeDelay;
			if (timeIntoFade >= fadeUpTime) {
				darkStart = DARK_START;
				darkEnd = DARK_END;
				isFadingUp = false;
				exempt->clear();
			} else {
				darkStart = DARK_START * (timeIntoFade / fadeUpTime);
				darkEnd = DARK_END * (timeIntoFade / fadeUpTime);
			}
		}
	}

	int keeperCenter[2];
	if (Keeper::theKeeper) {
		Keeper::theKeeper->getScreenCoords(keeperCenter);
		prevDarkX = keeperCenter[0];
		prevDarkY = keeperCenter[1];
	} else {
		keeperCenter[0] = prevDarkX;
		keeperCenter[1] = prevDarkY;
	}

	float alphaData[windowWidth * windowHeight];
	if (darkStart == 0) {
		for (int i = 0; i < windowWidth * windowHeight; i++) alphaData[i] = 1.0;
		glWindowPos2i(0, 0);
		glDrawPixels(windowWidth, windowHeight, GL_ALPHA, GL_FLOAT, alphaData);
		for (int i = 0; i < exempt->size(); i++) {
			exempt->at(i)->draw();
		}
		return;
	}


	int increment = DARK_RES_DECREASE * 2 + 1;
	for (int y = 0; y < (windowHeight + increment); y += increment) {
		for (int x = 0; x < (windowWidth + increment); x += increment) {
			int point[2];
			point[0] = x;
			point[1] = y;
			float dist = distBetweenPoints(point, keeperCenter);
			float alpha = fmin(fmax(dist - darkStart, 0.0) * 1.0 / (darkEnd - darkStart), 1.0);

			float alphaInv = 1.0 - alpha;
			alphaInv *= MIN_DEPRESSION_MULT + (1.0 - MIN_DEPRESSION_MULT) * happinessLevel;
			alpha = 1.0 - alphaInv;

			for (int trueY = y - DARK_RES_DECREASE; trueY <= y + DARK_RES_DECREASE; trueY++) {
				if (trueY < 0 || trueY >= windowHeight) continue;
				for (int trueX = x - DARK_RES_DECREASE; trueX <= x + DARK_RES_DECREASE; trueX++) {
					if (trueX < 0 || trueX >=windowWidth) continue;
					alphaData[trueY*windowWidth + trueX] = alpha;
				}
			}
		}
	}

	glWindowPos2i(0, 0);
	glDrawPixels(windowWidth, windowHeight, GL_ALPHA, GL_FLOAT, alphaData);
	for (int i = 0; i < exempt->size(); i++) {
		exempt->at(i)->draw();
	}
}

void NocturneCamera::computeBehavior(double timeLapsed) {

	NocturneObject::computeBehavior(timeLapsed);

	double happinessGoal = 1.0;
	if (Keeper::theKeeper && Keeper::theKeeper->ailment() == DEPRESSION) {
		happinessGoal = Keeper::theKeeper->getSanity();
	}
	if (happinessLevel > happinessGoal)
		happinessLevel = max(happinessGoal, happinessLevel - timeLapsed * HAPPINESS_FADE_SPEED);
	if (happinessLevel < happinessGoal)
		happinessLevel = min(happinessGoal, happinessLevel + timeLapsed * HAPPINESS_FADE_SPEED);
}

void NocturneCamera::makeExempt(NocturnePerson* person) {
	exempt->push_back(person);
}










