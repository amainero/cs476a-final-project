#include "Keeper.h"
#include <iostream>
#include "Room.h"
#include "Resident.h"
#include "Board.h"
#include "NocturneMessage.h"
#include "NoteLogic.h"

using namespace std;

Keeper* Keeper::theKeeper = NULL;
NocturneDialog* Keeper::spaaace = new NocturneDialog("./assets/DialogDefinitions/alone1.txt");

Keeper::Keeper() {
	color[0] = 1.0;
	color[1] = 0.0;
	color[2] = 0.0;
	restingColor[0] = 1.0;
	restingColor[1] = 0.0;
	restingColor[2] = 0.0;
	colorFadeSpeed = 0.2;

	Keeper::theKeeper = this;

	dirDown[0] = false;
	dirDown[1] = false;
	dirDown[2] = false;
	dirDown[3] = false;

	//playerAccelOn = false;

	collisionsEnabled = KEEPER_COLLISIONS;
	canChangeRooms = true;
	canGenerateArea = true;

	follower = NULL;
}

void Keeper::keyboardCallback(unsigned char key, int x, int y) { 
	if (key == 'q') exit(1);
	if (Keeper::theKeeper) Keeper::theKeeper->kCall(key, x, y);
}

void Keeper::kCall( unsigned char key, int x, int y ) {
	// TODO: For restricted movement, add check for if room has proper exit.

	switch( key )
    {
        case 'a':
        case 'A':
        	if (KEEPER_FREE_MOVEMENT) {
	        	if (!dirDown[LEFT]) x_accel += -ACCEL_PER_SECOND;
	        	/*if (!dirDown[LEFT]) {
	        		if (!playerAccelOn) {
	        			accel_strength += ACCEL_PER_SECOND;
	        			playerAccelOn = true;
	        		}
	        		accel_dir[0] += -1.0;
	        	}*/
        	} else {
        		if (currentMoveCommand == NULL && getRoom()->wallIsOpen(WEST)) commandToRelativeLoc(-ROOM_SIZE, 0, KEEPER_MOVE_COMMAND_TIME, SINE);
        	}
        	dirDown[LEFT] = true;
        	break;
        case 'w':
        case 'W':
        	if (KEEPER_FREE_MOVEMENT) {
	        	if (!dirDown[UP]) y_accel += ACCEL_PER_SECOND;
	            /*if (!dirDown[UP]) {
	        		if (!playerAccelOn) {
	        			accel_strength += ACCEL_PER_SECOND;
	        			playerAccelOn = true;
	        		}
	        		accel_dir[1] += 1.0;
	        	}*/
        	} else {
        		if (currentMoveCommand == NULL && getRoom()->wallIsOpen(NORTH)) commandToRelativeLoc(0, ROOM_SIZE, KEEPER_MOVE_COMMAND_TIME, SINE);
        	}
	        dirDown[UP] = true;
        	break;
        case 's':
        case 'S':
        	if (KEEPER_FREE_MOVEMENT) {
	        	if (!dirDown[DOWN]) y_accel += -ACCEL_PER_SECOND;
	            /*if (!dirDown[DOWN]) {
	        		if (!playerAccelOn) {
	        			accel_strength += ACCEL_PER_SECOND;
	        			playerAccelOn = true;
	        		}
	        		accel_dir[1] += -1.0;
	        	}*/
        	} else {
        		if (currentMoveCommand == NULL && getRoom()->wallIsOpen(SOUTH)) commandToRelativeLoc(0, -ROOM_SIZE, KEEPER_MOVE_COMMAND_TIME, SINE);
        	}
	        dirDown[DOWN] = true;
        	break;
        case 'd':
        case 'D':
        	if (KEEPER_FREE_MOVEMENT) {
	        	if (!dirDown[RIGHT]) x_accel += ACCEL_PER_SECOND;
	        	/*if (!dirDown[RIGHT]) {
	        		if (!playerAccelOn) {
	        			accel_strength += ACCEL_PER_SECOND;
	        			playerAccelOn = true;
	        		}
	        		accel_dir[0] += 1.0;
	        	}*/
        	} else {
        		if (currentMoveCommand == NULL && getRoom()->wallIsOpen(EAST)) commandToRelativeLoc(ROOM_SIZE, 0, KEEPER_MOVE_COMMAND_TIME, SINE);
        	}
	        dirDown[RIGHT] = true;
        	break;
        case 'f':
        case 'F':
        	acquireFollower();
			break;
		case 'u':
		case 'U':
			releaseFollower();
			break;
        case 'q':
            exit(1);
            break;
        case ' ':
        	// Keeper::spaaace->play();
        	/*if (ailment() == DEPRESSION) {
				restingColor[0] = 0.0;
				restingColor[1] = 0.0;
				restingColor[2] = 1.0;
			}*/
			sanity = 0.0;
        	break;
    }
}

void Keeper::keyboardUpCallback(unsigned char key, int x, int y) { if (Keeper::theKeeper) Keeper::theKeeper->kUpCall(key, x, y); }

void Keeper::kUpCall( unsigned char key, int x, int y ) {
	//bool dirChange = false;
	switch( key )
    {
        case 'a':
        case 'A':
        	if (KEEPER_FREE_MOVEMENT) {
	        	if (dirDown[LEFT]) x_accel -= -ACCEL_PER_SECOND;
	        	/*if (dirDown[LEFT]) {
	        		accel_dir[0] += 1.0;
	        		dirChange = true;
	        	}*/
	        }
	        dirDown[LEFT] = false;
        	break;
        case 'w':
        case 'W':
        	if (KEEPER_FREE_MOVEMENT) {
	        	if (dirDown[UP]) y_accel -= ACCEL_PER_SECOND;
	        	/*if (dirDown[UP]) {
	        		accel_dir[1] += -1.0;
	        		dirChange = true;
	        	}*/
	        }
	        dirDown[UP] = false;
        	break;
        case 's':
        case 'S':
        	if (KEEPER_FREE_MOVEMENT) {
	        	if (dirDown[DOWN]) y_accel -= -ACCEL_PER_SECOND;
	        	/*if (dirDown[DOWN]) {
	        		accel_dir[1] += 1.0;
	        		dirChange = true;
	        	}*/
	        }
	        dirDown[DOWN] = false;
        	break;
        case 'd':
        case 'D':
        	if (KEEPER_FREE_MOVEMENT) {
	        	if (dirDown[RIGHT]) x_accel -= ACCEL_PER_SECOND;
	        	/*if (dirDown[RIGHT]) {
	        		accel_dir[0] += -1.0;
	        		dirChange = true;
	        	}*/
	        }
	        dirDown[RIGHT] = false;
        	break;
    }

    /*if (dirChange
    	&& !dirDown[UP]
    	&& !dirDown[DOWN]
    	&& !dirDown[LEFT]
    	&& !dirDown[RIGHT]
    	&& playerAccelOn) {
    	accel_strength -= ACCEL_PER_SECOND;
    	playerAccelOn = false;
    }*/
}

void Keeper::computeBehavior(double timeLapsed) {
	if (ailment() == DEPRESSION) {
		restingColor[0] = 0.0;
		restingColor[1] = 0.0;
		restingColor[2] = 1.0;
	}
	if (ailment() == NO_AILMENT) {
		restingColor[0] = 1.0;
		restingColor[1] = 0.0;
		restingColor[2] = 0.0;
	}
	if (sanity <= 0.0) {
		restingColor[0] = 1.0;
		restingColor[1] = 1.0;
		restingColor[2] = 1.0;
	}
	if (color[0] == 1.0 && color[1] == 1.0 && color[2] == 1.0) {
		Resident *newResident = new Resident();
		Note root;
		root.semitone = 0;
		newResident->note = root;
		newResident->setLocation(x_loc, y_loc);
		newResident->setVel(x_vel, y_vel);
		newResident->updateCurrentRoom();
		newResident->startChirping();
		Keeper::theKeeper = NULL;
		delete this;
		return;
	}

	// Super behavior.
	NocturnePerson::computeBehavior(timeLapsed);

	// See if the quest should be advanced.
	if (getArea()->checkQuest()) {
		sanity += KEEPER_SANITY_REWARD;
		if (sanity > 1.0) sanity = 1.0;
	}
}


void Keeper::onRoomChange(Room *newRoom) {
	if (sanity < 0.0) sanity = 0.0;

	// TODO: Try to get this to work.
	if (!newRoom) {

		// create a new area
		int keeper_grid_coordinate_x = NocturneObject::theBoard->getGridCoordinateFromRawCoordinateX(getX());
    	int keeper_grid_coordinate_y = NocturneObject::theBoard->getGridCoordinateFromRawCoordinateY(getY());

    	int cur_room_x = oldRoom->getAbsoluteX();
        int cur_room_y = oldRoom->getAbsoluteY();

        Area *new_area = NULL;

        if (keeper_grid_coordinate_x < cur_room_x) {
            // dir: WEST
            new_area = NocturneObject::theBoard->constructArea(oldRoom, WEST);
        } else if (keeper_grid_coordinate_x > cur_room_x) {
            // dir: EAST
            new_area = NocturneObject::theBoard->constructArea(oldRoom, EAST);
        } else if (keeper_grid_coordinate_y < cur_room_y) {
            // dir: SOUTH
            new_area = NocturneObject::theBoard->constructArea(oldRoom, SOUTH);
        } else if (keeper_grid_coordinate_y > cur_room_y) {
            // dir: NORTH
            new_area = NocturneObject::theBoard->constructArea(oldRoom, NORTH);
        }
        newRoom = NocturneObject::theBoard->getRoomFromGridCoordinates(keeper_grid_coordinate_x, keeper_grid_coordinate_y);
	}


	vector<Resident *> residentsInNewRoom = newRoom->getResidents();
	
	if (oldRoom) {
		vector<Resident *> residentsInOldRoom = oldRoom->getResidents();

		for (int i = 0; i < residentsInOldRoom.size(); i++) {
			residentsInOldRoom[i]->stopChirping();
		}
	}

	for (int i = 0; i < residentsInNewRoom.size(); i++) {
		if (!follower) {
			residentsInNewRoom[i]->sing();
		}
		residentsInNewRoom[i]->startChirping();
	}

	if (!getArea()->questComplete()) sanity -= KEEPER_SANITY_LOSS;
	if (getArea()->questComplete()) myAilment = NO_AILMENT;
	else myAilment = getArea()->getAilment();
}


void Keeper::acquireFollower() {
	Resident *new_follower = NULL;
	vector<Resident *> residents_in_room = getRoom()->getResidents();
	
	if (residents_in_room.size() == 0) {
		return;
	}

	for (int i = 0; i < residents_in_room.size() - 1; i++) {
		if (residents_in_room[i] == follower) {
			new_follower = residents_in_room[i + 1];
		}
	}
	if (!new_follower) {
		new_follower = residents_in_room[0];
	}

	if (follower) {
		follower->stopFollowingKeeper();
	}
	follower = new_follower;
	follower->followKeeper();
}


void Keeper::releaseFollower() {
	if (follower) {
		follower->stopFollowingKeeper();
	}
	follower = NULL;
}
