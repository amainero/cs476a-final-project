#ifdef __MACOSX_CORE__
  // note: for mac only
  #include <GLUT/glut.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
  #include <GL/glut.h>
#endif

#include <string>
#include <vector>
#include <map>
#include "Timing.h"
#include "NocturneCamera.h"

#ifndef NOCTURNE_MESSAGE_H
#define NOCTURNE_MESSAGE_H

using namespace std;

#define DEFAULT_FADE_TIME 1.0

#define CHATTER_TIME 3.0
#define CHATTER_OFFSET 0.15

using namespace std;

class NocturnePerson;

class NocturneMessage {
	public:
		static void chatter(double x, double y, string message, double timeIntoChatter);

		NocturneMessage(string message, void* font, double duration, double xFrac, double yFrac);
		void play();
		double totalTime();
		void setDelay(double delay);
		void setFinishLoc(double xFrac, double yFrac);
		void setSelfDestruct(bool destruct);
		void setMoveTime(double moveTime);

		static void computeFrame();
		static NocturneCamera* theCamera;

		static double intro();
	private:
		static vector<NocturneMessage*> activeMessages;

		double delay, fadeIn, sustain, fadeOut;
		double moveTime;
		double startTime;
		string message;
		void* font;
		double xFracStart, yFracStart, xFracEnd, yFracEnd;
		bool thisMessageWillSelfDestruct;

		static int widthOfMessage(void* font, string message);
		int messageWidth();
		bool updateAndCheckComplete(double theTime);
};

class NocturneDialog {
public:
	NocturneDialog(string filename);
	~NocturneDialog();
	double play();
	string getID();
	static NocturneDialog* getDialog(string id);
private:
	vector<NocturneMessage*> messages;
	vector<string> exemptions;
	string id;
};

#endif